import { AlertService, MessageSeverity } from './services/alert.service';
import { ErrorHandler, Injectable, Injector } from '@angular/core';

@Injectable()
export class AppErrorHandler extends ErrorHandler {

    constructor(private alertService: AlertService) {
        super();
    }


    handleError(error: any) {

        // this.alertService.showStickyMessage("Fatal Error!", "An unresolved error has occured. Please reload the page to correct this error", MessageSeverity.warn);
        this.alertService.showStickyMessage(error.message || error,"Unhandled Error",  MessageSeverity.error, error);

        // if (confirm('Fatal Error!\nAn unresolved error has occured. Do you want to reload the page to correct this?\n\nError: ' + error.message)) {
        //     window.location.reload();
        // }

        super.handleError(error);
    }
}
