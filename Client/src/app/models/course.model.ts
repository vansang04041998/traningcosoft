
export class Course {
    constructor(id?: string, name?: string, image?: string, lecturers?: string, code?: string, description?: string, fee?: number, numberOfLesson?: number, scheduleDate?: Date, startTime?: any, endTime?: any) {

        this.id = id;
        this.name = name;
        this.image = image;
        this.lecturers = lecturers;
        this.code = code;
        this.description = description;
        this.fee = fee;
        this.numberOfLesson = numberOfLesson;
        this.scheduleDate = scheduleDate;
        this.startTime = startTime;
        this.endTime = endTime;
    }
 

    public id?: string;
    public name?: string;
    public image?: string;
    public lecturers?: string;
    public code?: string;
    public description?: string;
    public fee?: number;
    public numberOfLesson?: number;
    public scheduleDate?: Date;
    public startTime?: any;
    public endTime?: any;
}
