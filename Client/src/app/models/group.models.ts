import { User } from "./user.model";

export class Group {

    constructor(name?: string, description?: string, users?: User[]) {

        this.name = name;
        this.description = description;
        this.users = users;
    }

    public id?: string;
    public name?: string;
    public description?: string;
    public users?: User[];
}
