import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'mapKey'
})
export class MapKeyPipe implements PipeTransform {

    transform(arr: Array<any>, field: string): Array<any> {

        if (!arr) {
            return arr;
        }

        return arr.map(o => o[field]);
    }
}
