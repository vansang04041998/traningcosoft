import { Pipe, PipeTransform } from '@angular/core';

import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
    name: 'safe'
})
export class SafePipe implements PipeTransform {

    constructor(private _sanitizer: DomSanitizer) { }

    transform(url: string, type: 'url' | 'html' = 'url') {
        switch (type) {
            case 'html': return this._sanitizer.bypassSecurityTrustHtml(url);
            case 'url': return this._sanitizer.bypassSecurityTrustResourceUrl(url);
            default: return url;
        }
    }

}
