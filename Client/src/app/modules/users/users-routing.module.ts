import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from 'src/app/services/auth-guard.service';
import { GroupComponent } from './group/group.component';
import { ListComponent } from './list/list.component';
import { MembersComponent } from './members/members.component';
import { NgModule } from '@angular/core';
import { RolesComponent } from './roles/roles.component';


const routes: Routes = [
  {
    path: '',
    component: ListComponent, canActivate: [AuthGuard], data: { title: 'User Management' }
  },
  {
    path: 'group',
    component: GroupComponent, canActivate: [AuthGuard], data: { title: 'Membership Group' }
  },
  {
    path: 'roles',
    component: RolesComponent, canActivate: [AuthGuard], data: { title: 'Roles Management' }
  },
  {
    path: 'members',
    component: MembersComponent, canActivate: [AuthGuard], data: { title: 'Members Management' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
