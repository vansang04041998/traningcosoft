import { CommonModule } from '@angular/common';
import { GroupComponent } from './group/group.component';
import { ListComponent } from './list/list.component';
import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { UsersRoutingModule } from './users-routing.module';
import { RolesComponent } from './roles/roles.component';
import { MembersComponent } from './members/members.component';


@NgModule({
  declarations: [
    ListComponent,
    GroupComponent,
    RolesComponent,
    MembersComponent
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    SharedModule
  ]
})
export class UsersModule { }
