import { Component, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent, DxFormComponent } from 'devextreme-angular';

import { AbstractListComponent } from 'src/app/shared/components/abstract.component';
import ArrayStore from 'devextreme/data/array_store';
import CustomStore from 'devextreme/data/custom_store';
import { HttpParams } from '@angular/common/http';
import { Injector } from '@angular/core';
import { Role } from 'src/app/models/role.model';
import { UserEdit } from 'src/app/models/user-edit.model';
import { Utilities } from 'src/app/services/utilities';
import { clone } from 'lodash';
import { custom } from 'devextreme/ui/dialog';
import { fadeInOut } from 'src/app/services/animations';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss'],
  animations: [fadeInOut],
})
export class MembersComponent extends AbstractListComponent implements OnInit {

  @ViewChild(DxDataGridComponent, { static: false }) dataSourceGrid!: DxDataGridComponent;
  @ViewChild(DxFormComponent, { static: false }) taskForm!: DxFormComponent;

  accountStatus = [
    { value: true, display: 'Active' },
    { value: false, display: 'Locked' }
  ];

  genders = [
    'Male',
    'Female',
    'Unknown'
  ];

  isClearFilter = true;

  memberFeesDataSource: any;
  memberFeesFilter = [
    { text: 'Not yet', value: ['MemberShip', '=', 0] },
    { text: '3 year', value: ['MemberShip', '=', 1] },
    { text: '6 year', value: ['MemberShip', '=', 2] },
    { text: '9 year', value: ['MemberShip', '=', 3] },
  ];
  memberFees = [
    { name: 'Not yet', value: null },
    { name: '3 year', value: 1 },
    { name: '6 year', value: 2 },
    { name: '9 year', value: 3 },
  ];

  rolesFilter: any = [];

  constructor(injector: Injector) {
    super(injector);

    this.ayncValidate = this.ayncValidate.bind(this);
    this.onCustomRule = this.onCustomRule.bind(this);
  }

  ngOnInit(): void {
    const self = this;

    this.dataSource = new CustomStore({
      key: 'id',
      load: async (loadOptions: any) => {
        let params: HttpParams = new HttpParams();

        if (loadOptions.filter) {
          loadOptions.filter = [loadOptions.filter];
          loadOptions.filter.push('and');
          loadOptions.filter.push(['isStaff', '=', false]);
        } else {
          loadOptions.filter = ['isStaff', '=', false];
        }

        this.queryParams.forEach((i) => {
          if (i in loadOptions && this.isNotEmpty(loadOptions[i])) params = params.set(i, JSON.stringify(loadOptions[i]));
        });

        return this.accountService.getAllUsers(params).toPromise();
      }
    });

    self.accountService.getGroups().subscribe(resp => {
      self.rolesFilter = [];
      (resp.data as Array<any>).map(e => self.rolesFilter.push({ text: e.name, value: ['groupString', 'contains', e.name] }));
    });

    this.memberFeesDataSource = new CustomStore({
      key: "id",
      load: async (loadOptions: any) => {
        let params: HttpParams = new HttpParams();
        this.queryParams.forEach((i) => {
          if (i in loadOptions && this.isNotEmpty(loadOptions[i])) params = params.set(i, JSON.stringify(loadOptions[i]));
        });
        return self.accountService.getMemberFees(params).toPromise();
      }
    });
  }

  async onToolbarPreparing(e: any) {
    const self = this;
    e.toolbarOptions.items.unshift(
      {
        location: 'before',
        widget: 'dxButton',
        options: {
          template: 'deleteButton',
          onClick: this.onRowEdit = this.onRowEdit.bind(this)
        }
      },
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          icon: 'upload',
          hint: 'Import Excel',
          stylingMode: 'contained',
          onClick: this.onClearFiler = this.onClearFiler.bind(this)
        }
      },
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          icon: 'refresh',
          hint: 'Refresh Filter',
          stylingMode: 'contained',
          onClick: this.onClearFiler = this.onClearFiler.bind(this)
        }
      },
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          icon: 'add',
          hint: 'Add new member',
          type: 'default', stylingMode: 'contained',
          onClick: this.onRowEdit = this.onRowEdit.bind(this)
        }
      }
    );
  }

  async onClearFiler() {
    this.dataSourceGrid.instance.clearFilter();
    this.isClearFilter = true;
  }

  async onSave(e: any) {
    e.preventDefault();
    if (this.obj.id) {
      this.obj.roles = ['User'];
      this.accountService.updateUser(this.obj).subscribe(resp => {
        this.popupVisible = false;
        this.dataSourceGrid.instance.refresh();
      }, error => console.log(error));
    } else {
      this.obj.userName = 'SHA' + Utilities.uniqueId();
      this.obj.newPassword = this.obj.confirmPassword = this.obj.currentPassword = Utilities.generatePassword();
      this.obj.configuration = '';
      this.obj.roles = ['User'];
      this.obj.isStaff = false;
      this.accountService.newUser(this.obj).subscribe(resp => {
        this.popupVisible = false;
        this.dataSourceGrid.instance.refresh();
      }, error => console.log(error));
    }
  }

  async onRowEdit(cell: any = null) {
    this.obj = (cell && cell.row && cell.row.data) ? clone(cell.row.data) : new UserEdit();
    this.popupVisible = !this.popupVisible;
  }

  async onRowDelete(e: any) {
    custom({
      messageHtml: 'Are you sure you want to delete this record ?',
      title: 'Confirm',
      buttons: [
        { text: 'Yes, delete it!', stylingMode: 'contained', type: 'default', onClick: (e) => true },
        { text: 'Cancel', type: 'normal', onClick: (e) => false }
      ]
    }).show().then(async (isSubmit: boolean) => {
      if (isSubmit) {
        this.accountService.deleteUser(e.key).subscribe(resp => {
          e.component.refresh();
        });
        // await this.dataSource.remove(e.key);

      }
    });
  }

  async onCustomRule() {
    return false;
  }

  valueChanged(e: any) {
    // this.emailValue = data.value.replace(/\s/g, '').toLowerCase() + '@corp.com';
    console.log(e.component, e.component._changedValue, this.obj);
  }
}
