import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from 'src/app/services/auth-guard.service';
import { ListComponent } from './list/list.component';
import { NgModule } from '@angular/core';
import { OrdersComponent } from './orders/orders.component';

const routes: Routes = [
  {
    path: '',
    component: ListComponent, canActivate: [AuthGuard], data: { title: 'Class Management' }
  },
  {
    path: 'orders',
    component: OrdersComponent, canActivate: [AuthGuard], data: { title: 'Orders Management' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClassRoomsRoutingModule { }
