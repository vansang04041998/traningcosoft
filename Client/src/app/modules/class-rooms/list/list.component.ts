import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent, DxFormComponent } from 'devextreme-angular';
import { clone, has } from 'lodash';

import { AbstractListComponent } from 'src/app/shared/components/abstract.component';
import { ClassRoom } from 'src/app/models/class-room.model';
import { Course } from 'src/app/models/course.model';
import { CourseService } from 'src/app/services/course.service';
import CustomStore from 'devextreme/data/custom_store';
import { HttpParams } from '@angular/common/http';
import { fadeInOut } from 'src/app/services/animations';

@Component({
  selector: 'app-class-rooms-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  animations: [fadeInOut],
})
export class ListComponent extends AbstractListComponent implements OnInit {

  @ViewChild(DxDataGridComponent, { static: false }) dataSourceGrid!: DxDataGridComponent;
  @ViewChild(DxFormComponent, { static: false }) taskForm!: DxFormComponent;

  statusModes = [
    { id: 1, name: 'Opening' },
    { id: 2, name: 'In Progress' },
    { id: 3, name: 'Pending' },
    { id: 4, name: 'Ended' }
  ];

  classStatusFilter = [
    { text: 'Opening', value: ['Status', '=', 1] },
    { text: 'In Progress', value: ['Status', '=', 2] },
    { text: 'Pending', value: ['Status', '=', 3] },
    { text: 'Ended', value: ['Status', '=', 4] },
  ];

  coursesFilter = [];
  courses: any = [];

  constructor(injector: Injector, private courseService: CourseService) {
    super(injector);

    this.ayncValidate = this.ayncValidate.bind(this);
  }

  ngOnInit(): void {
    const self = this;

    this.dataSource = new CustomStore({
      key: 'id',
      load: async (loadOptions: any) => {
        let params: HttpParams = new HttpParams();

        self.queryParams.forEach((i) => {
          if (i in loadOptions && self.isNotEmpty(loadOptions[i])) params = params.set(i, JSON.stringify(loadOptions[i]));
        });

        return self.courseService.getAllClassRooms(params).toPromise();
      },
      remove: async (id) => {
        this.courseService.deleteClassRoom(id).subscribe(() => this.saveSuccessHelper(this.obj), (error: any) => this.saveFailedHelper(error));
      }
    });

    self.courseService.getAllCourses().subscribe(resp => {
      self.coursesFilter = [];
      (resp.data as Array<any>).map(e => self.courses.push({ id: e.id, name: e.name }));
    });
  }

  async onRowEdit(cell: any = null) {
    this.popupVisible = !this.popupVisible;
    this.obj = new ClassRoom();

    if (this.taskForm) this.taskForm.instance.resetValues();
    if (this.taskForm && (cell && cell.row && cell.row.data)) {
      var o = clone(cell.row.data);
      o.startTime = new Date(`1970-01-01 ${cell.row.data.startTime.hours}:${cell.row.data.startTime.minutes}`);
      o.endTime = new Date(`1970-01-01 ${cell.row.data.endTime.hours}:${cell.row.data.endTime.minutes}`);
      this.taskForm.formData = this.obj = o;
    }
  }

  async onSave(e: any) {
    e.preventDefault();

    if (this.taskForm.instance.validate().isValid) {
      (has(this.obj, 'id') && this.obj.id) ?
        this.courseService.updateClassRoom(clone(this.taskForm.formData)).subscribe(() => this.saveSuccessHelper(this.taskForm.formData), (error: any) => this.saveFailedHelper(error)) :
        this.courseService.newClassRoom(this.taskForm.formData).subscribe(() => this.saveSuccessHelper(this.taskForm.formData), (error: any) => this.saveFailedHelper(error));

      this.popupVisible = false;
    }
  }

  private saveSuccessHelper(obj?: any) {

    this.popupVisible = false;
    this.dataSourceGrid.instance.refresh();

    if (this.changesSavedCallback) {
      this.changesSavedCallback();
    }
  }
}
