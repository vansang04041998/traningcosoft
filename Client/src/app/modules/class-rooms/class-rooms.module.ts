import { ClassRoomsRoutingModule } from './class-rooms-routing.module';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { OrdersComponent } from './orders/orders.component';

@NgModule({
  declarations: [
    ListComponent,
    OrdersComponent
  ],
  imports: [
    CommonModule,
    ClassRoomsRoutingModule,
    SharedModule
  ]
})
export class ClassRoomsModule { }
