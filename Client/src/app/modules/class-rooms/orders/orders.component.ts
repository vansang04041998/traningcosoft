import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent, DxFormComponent } from 'devextreme-angular';
import { clone, has, map } from 'lodash';

import { AbstractListComponent } from 'src/app/shared/components/abstract.component';
import { CourseService } from 'src/app/services/course.service';
import CustomStore from 'devextreme/data/custom_store';
import { Group } from 'src/app/models/group.models';
import { HttpParams } from '@angular/common/http';
import { custom } from 'devextreme/ui/dialog';
import { fadeInOut } from 'src/app/services/animations';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss'],
  animations: [fadeInOut],
})
export class OrdersComponent extends AbstractListComponent implements OnInit {

  @ViewChild(DxDataGridComponent, { static: false }) dataSourceGrid!: DxDataGridComponent;
  @ViewChild(DxFormComponent, { static: false }) taskForm!: DxFormComponent;

  members: any;
  classRooms: any;
  popupMemberVisible = false;

  classRoomsData: any;
  membersData: any;

  classStatusFilter = [
    { text: 'Opening', value: ['ClassRoom.Status', '=', 1] },
    { text: 'In Progress', value: ['ClassRoom.Status', '=', 2] },
    { text: 'Pending', value: ['ClassRoom.Status', '=', 3] },
    { text: 'Ended', value: ['ClassRoom.Status', '=', 4] },
  ];

  constructor(injector: Injector, private courseService: CourseService) {
    super(injector);

    this.ayncValidate = this.ayncValidate.bind(this);
    this.onSelectValueChanged = this.onSelectValueChanged.bind(this);
  }

  ngOnInit(): void {
    const self = this;

    this.dataSource = new CustomStore({
      key: 'id',
      load: async (loadOptions: any) => {
        let params: HttpParams = new HttpParams();

        self.queryParams.forEach((i) => {
          if (i in loadOptions && self.isNotEmpty(loadOptions[i])) params = params.set(i, JSON.stringify(loadOptions[i]));
        });
        return self.courseService.getAllOrders(params).toPromise();
      },
      remove: async (id) => {
        this.courseService.deleteOrder(id).subscribe(() => this.saveSuccessHelper(this.obj), (error: any) => this.saveFailedHelper(error));
      }
    });

    this.members = new CustomStore({
      key: 'id',
      load: async (loadOptions: any) => {
        let params: HttpParams = new HttpParams();
        if (loadOptions.filter) {
          loadOptions.filter.push(['isStaff', '=', false]);
        } else {
          loadOptions.filter = ['isStaff', '=', false];
        }
        this.queryParams.forEach((i) => {
          if (i in loadOptions && this.isNotEmpty(loadOptions[i])) params = params.set(i, JSON.stringify(loadOptions[i]));
        });
        return this.accountService.getAllUsers(params).toPromise();
      }
    });

    this.classRooms = new CustomStore({
      key: 'id',
      load: async (loadOptions: any) => {
        let params: HttpParams = new HttpParams();
        // if (loadOptions.filter) {
        //   loadOptions.filter.push(['status', '<>', 2]);
        // } else {
        //   loadOptions.filter = ['status', '<>', 2];
        // }
        this.queryParams.forEach((i) => {
          if (i in loadOptions && this.isNotEmpty(loadOptions[i])) params = params.set(i, JSON.stringify(loadOptions[i]));
        });

        return this.courseService.getAllClassRooms(params).toPromise();
      }
    });
  }

  async onSave(e: any) {
    e.preventDefault();

    if (this.taskForm.instance.validate().isValid) {
      has(this.obj, 'id') ?
        this.courseService.updateOrder(this.obj).subscribe(() => this.saveSuccessHelper(this.obj), (error: any) => this.saveFailedHelper(error)) :
        this.courseService.newOrder(this.obj).subscribe(() => this.saveSuccessHelper(this.obj), (error: any) => this.saveFailedHelper(error));

      await this.dataSourceGrid.instance.refresh();

      this.popupVisible = false;
    }
  }

  async onRowDelete(e: any) {
    custom({
      messageHtml: 'Are you sure you want to cancel this order ?',
      title: 'Confirm',
      buttons: [
        { text: 'Yes, sure !', stylingMode: 'contained', type: 'default', onClick: (e) => true },
        { text: 'Cancel', type: 'normal', onClick: (e) => false }
      ]
    }).show().then(async (isSubmit: boolean) => {
      if (isSubmit) {
        await this.dataSource.remove(e.key);
        await e.component.refresh();
      }
    });
  }

  onSelectValueChanged(e: any) {
    if (!e || !e.value) return;
    console.log(e.value);

    if (e.value.indexOf('SHAI-HY') >= 0) {
      this.courseService.getOneClassRoom(e.value).subscribe((res: any) => this.classRoomsData = res);
    } else {
      this.accountService.getUser(e.value).subscribe(res => this.membersData = res);
    }
  }

  private saveSuccessHelper(group?: Group) {

    this.popupVisible = false;
    this.dataSourceGrid.instance.refresh();

    if (this.changesSavedCallback) {
      this.changesSavedCallback();
    }
  }
}