import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/services/auth-guard.service';
import { ListComponent } from './list/list.component';
import { AddComponent } from './add/add.component';


const routes: Routes = [
  {
    path: '',
    component: ListComponent, data: { title: 'Quản lí sinh viên' }
  },
  {
    path: 'add',
    component: AddComponent,  data: { title: 'Tạm thời chưa muốn code' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SinhvienRoutingModule { }
