import { Component, OnInit,Injector,ViewChild} from '@angular/core';
import { ary } from 'lodash';
import { AbstractListComponent } from 'src/app/shared/components/abstract.component';
import { SinhVien ,SinhVien1} from '../../../models/sinhvien.model';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import {FormGroup,FormBuilder}  from '@angular/forms';
import { SinhvienserviceService } from 'src/app/services/sinhvienservice.service';
import { Parser } from '@angular/compiler/src/ml_parser/parser';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss'],

})
export class AddComponent extends AbstractListComponent implements OnInit {
  constructor(injector: Injector, private httpservice: SinhvienserviceService, private fb: FormBuilder) {
    super(injector);

    this.ayncValidate = this.ayncValidate.bind(this);
  }
  //
  public data: SinhVien = new SinhVien1();
  ///
  public add() {
    this.data = <SinhVien> this.form123.value;
    // console.log(this.data)
    //
    // this.httpservice.postnew(this.data).subscribe((data:any)=>{ 
          // console.log(this.form123);
    //  });
     this.httpservice.postnew(this.form123.value).subscribe(() => this.saveSuccessHelper(this.taskForm.formData), (error: any) => this.saveFailedHelper(error));
   }
   private saveSuccessHelper(obj?: any) {

    this.popupVisible = false;
    this.dataSourceGrid.instance.refresh();

    if (this.changesSavedCallback) {
      this.changesSavedCallback();
    }
  }
  form123: FormGroup = this.fb.group({
    hoTen : this.data.hoTen,
    ngaysinh:this.data.ngaysinh,
    gioiTinh : this.data.gioiTinh,
    soThich :this.data.soThich,
    hocLuc : this.data.hocLuc,
    sdt :this.data.sdt,
});
  public form1234 = new FormGroup({
    data : new FormControl(''),
    ngaysinh : new FormControl(Date),
    gioiTinh : new FormControl(Int32Array),
    soThich : new FormControl(''),
    hocLuc : new FormControl(''),
    sdt : new FormControl(Int32Array),

  });
  ngOnInit(): void {
    const self = this;
    //
  //   this.httpservice.getdata().subscribe((data:any)=>{ 
  //     console.log("Đây là dữ liệu :" ,data);
  //     console.log(this.form123);
  //     this.Dulieu = data;
  // });
  //
  
  }
}
