import { Component, OnInit } from '@angular/core';
import { fadeInOut } from 'src/app/services/animations';
import { SinhVien } from '../../../models/sinhvien.model';
import { SinhvienserviceService } from 'src/app/services/sinhvienservice.service';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { forEach } from 'lodash';

@Component({
  animations: [fadeInOut],
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  
  Dulieu: SinhVien[] = [];
   closeModal : any;
   id ='';
  constructor(public httpservice :SinhvienserviceService) { }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  ngOnInit(): void {
  this.httpservice.getdata().subscribe((data:any)=>{ 
      console.log("Đây là dữ liệu :" ,data);
      this.Dulieu = data;
  });
  }
  deleteEmp(id:any): void {
    this.httpservice.delete(id)
     if (confirm("Are you sure you want to delete " + this.id + "?")){
    console.log();
    }
  }
}
