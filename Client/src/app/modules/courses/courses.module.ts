import { CommonModule } from '@angular/common';
import { CoursesRoutingModule } from './courses-routing.module';
import { GenresComponent } from './genres/genres.component';
import { ListComponent } from './list/list.component';
import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    ListComponent,
    GenresComponent
  ],
  imports: [
    CommonModule,
    CoursesRoutingModule,
    SharedModule
  ]
})
export class CoursesModule { }
