import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent, DxFormComponent } from 'devextreme-angular';
import { clone, has } from 'lodash';

import { AbstractListComponent } from 'src/app/shared/components/abstract.component';
import { Course } from 'src/app/models/course.model';
import { CourseService } from 'src/app/services/course.service';
import CustomStore from 'devextreme/data/custom_store';
import { HttpParams } from '@angular/common/http';
import { fadeInOut } from 'src/app/services/animations';

@Component({
  selector: 'app-genres',
  templateUrl: './genres.component.html',
  styleUrls: ['./genres.component.scss'],
  animations: [fadeInOut],
})
export class GenresComponent extends AbstractListComponent implements OnInit {

  @ViewChild(DxDataGridComponent, { static: false }) dataSourceGrid!: DxDataGridComponent;
  @ViewChild(DxFormComponent, { static: false }) taskForm!: DxFormComponent;

  constructor(injector: Injector, private courseService: CourseService) {
    super(injector);

    this.ayncValidate = this.ayncValidate.bind(this);
  }

  ngOnInit(): void {
    const self = this;

    this.dataSource = new CustomStore({
      key: 'id',
      load: async (loadOptions: any) => {
        let params: HttpParams = new HttpParams();

        self.queryParams.forEach((i) => {
          if (i in loadOptions && self.isNotEmpty(loadOptions[i])) params = params.set(i, JSON.stringify(loadOptions[i]));
        });

        return self.courseService.getAllGenres(params).toPromise();
      },
      remove: async (id) => {
        this.courseService.deleteGenre(id).subscribe(() => this.saveSuccessHelper(this.obj), (error: any) => this.saveFailedHelper(error));
      }
    });
  }

  async onSave(e: any) {
    e.preventDefault();

    if (this.taskForm.instance.validate().isValid) {
      has(this.obj, 'id') ?
        this.courseService.updateGenre(this.obj).subscribe(() => this.saveSuccessHelper(this.obj), (error: any) => this.saveFailedHelper(error)) :
        this.courseService.newGenre(this.obj).subscribe(() => this.saveSuccessHelper(this.obj), (error: any) => this.saveFailedHelper(error));

      await this.dataSourceGrid.instance.refresh();

      this.popupVisible = false;
    }
  }

  // async onRowEdit(cell: any = null) {
  //   this.popupVisible = !this.popupVisible;
  //   this.obj = new Course();

  //   if (this.taskForm) this.taskForm.instance.resetValues();
  //   if (this.taskForm && (cell && cell.row && cell.row.data)) {
  //     var o = clone(cell.row.data);
  //     o.startTime = new Date(`1970-01-01 ${cell.row.data.startTime.hours}:${cell.row.data.startTime.minutes}`);
  //     o.endTime = new Date(`1970-01-01 ${cell.row.data.endTime.hours}:${cell.row.data.endTime.minutes}`);
  //     this.taskForm.formData = this.obj = o;
  //   }
  // }

  // async onSave(e: any) {
  //   e.preventDefault();
  //   console.log(this.obj);
  // }

  private saveSuccessHelper(obj?: any) {

    this.popupVisible = false;
    this.dataSourceGrid.instance.refresh();

    if (this.changesSavedCallback) {
      this.changesSavedCallback();
    }
  }
}
