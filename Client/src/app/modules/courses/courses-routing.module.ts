import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from 'src/app/services/auth-guard.service';
import { GenresComponent } from './genres/genres.component';
import { ListComponent } from './list/list.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    component: ListComponent, canActivate: [AuthGuard], data: { title: 'Course Management' }
  },
  {
    path: 'genres',
    component: GenresComponent, canActivate: [AuthGuard], data: { title: 'Genre Management' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoursesRoutingModule { }
