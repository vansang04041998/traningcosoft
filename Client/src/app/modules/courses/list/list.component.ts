import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent, DxFormComponent } from 'devextreme-angular';
import { clone, has } from 'lodash';

import { AbstractListComponent } from 'src/app/shared/components/abstract.component';
import { Course } from 'src/app/models/course.model';
import { CourseService } from 'src/app/services/course.service';
import CustomStore from 'devextreme/data/custom_store';
import { HttpParams } from '@angular/common/http';
import { fadeInOut } from 'src/app/services/animations';

@Component({
  selector: 'app-course-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  animations: [fadeInOut],
})
export class ListComponent extends AbstractListComponent implements OnInit {

  @ViewChild(DxDataGridComponent, { static: false }) dataSourceGrid!: DxDataGridComponent;
  @ViewChild(DxFormComponent, { static: false }) taskForm!: DxFormComponent;

  genresFilter: any = [];
  genres: any = [];

  languagesFilter: any = [
    { text: 'English', value: ['language', '=', 'English'] },
    { text: 'Chinese', value: ['language', '=', 'Chinese'] }
  ];

  constructor(injector: Injector, private courseService: CourseService) {
    super(injector);

    this.ayncValidate = this.ayncValidate.bind(this);
  }

  ngOnInit(): void {
    const self = this;

    self.dataSource = new CustomStore({
      key: 'id',
      load: async (loadOptions: any) => {
        let params: HttpParams = new HttpParams();

        self.queryParams.forEach((i) => {
          if (i in loadOptions && self.isNotEmpty(loadOptions[i])) params = params.set(i, JSON.stringify(loadOptions[i]));
        });

        return self.courseService.getAllCourses(params).toPromise();
      },
      remove: async (id) => {
        this.courseService.deleteCourse(id).subscribe(() => this.saveSuccessHelper(this.obj), (error: any) => this.saveFailedHelper(error));
      }
    });

    self.courseService.getAllGenres().subscribe(resp => {
      self.genresFilter = [];
      (resp.data as Array<any>).map(e => self.genresFilter.push({ text: e.name, value: ['genreId', '=', e.id] }));

      (resp.data as Array<any>).map(e => self.genres.push({ id: e.id, name: e.name }));
    });

  }

  async onToolbarPreparing(e: any) {
    if (this.isGrant('courses.manage')) {
      e.toolbarOptions.items.unshift(
        {
          location: 'after',
          widget: 'dxButton',
          options: {
            icon: 'add',
            type: 'default', stylingMode: 'contained',
            onClick: this.onRowEdit = this.onRowEdit.bind(this)
          }
        });
    }

    e.toolbarOptions.items.unshift(
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          icon: 'refresh',
          stylingMode: 'contained',
          disabled: this.isGrant('courses.manage'),
          onClick: this.onClearFiler = this.onClearFiler.bind(this)
        }
      });

  }

  async onSave(e: any) {
    e.preventDefault();

    if (this.taskForm.instance.validate().isValid) {
      has(this.obj, 'id') ?
        this.courseService.updateCourse(clone(this.taskForm.formData)).subscribe(() => this.saveSuccessHelper(this.taskForm.formData), (error: any) => this.saveFailedHelper(error)) :
        this.courseService.newCourse(this.taskForm.formData).subscribe(() => this.saveSuccessHelper(this.taskForm.formData), (error: any) => this.saveFailedHelper(error));

      this.popupVisible = false;
    }
  }

  private saveSuccessHelper(obj?: any) {

    this.popupVisible = false;
    this.dataSourceGrid.instance.refresh();

    if (this.changesSavedCallback) {
      this.changesSavedCallback();
    }
  }

}
