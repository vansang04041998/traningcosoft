import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
    selector: 'img[default]',
    host: { '(error)': 'updateUrl()', '(load)': 'load()', '[src]': 'src' }
})

export class ImagePreloadDirective {
    @Input() src: string | undefined;
    @Input() default: string | undefined;
    @HostBinding('class') className: string | undefined;

    updateUrl() {
        this.src = this.default;
    }
    load() {
        this.className = 'image-loaded';
    }
}
