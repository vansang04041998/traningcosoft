import { Component } from '@angular/core';
import { fadeInOut } from 'src/app/services/animations';

@Component({
  templateUrl: 'home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [fadeInOut]
})

export class HomeComponent {
  constructor() { }
}
