import { AlertService, MessageSeverity } from "src/app/services/alert.service";
import { InjectionToken, Injector } from "@angular/core";

import { AccountService } from "src/app/services/account.service";
import { AppConsts } from "src/app/services/db-keys";
import { AppTitleService } from "src/app/services/app-title.service";
import { AuthService } from "src/app/services/auth.service";
import { ConfigurationService } from "src/app/services/configuration.service";
import { DxDataGridComponent } from "devextreme-angular";
import { HttpClient } from "@angular/common/http";
import { User } from "src/app/models/user.model";
import { Utilities } from "src/app/services/utilities";
import { clone } from "lodash";
import { custom } from "devextreme/ui/dialog";

export abstract class AbstractComponent {


    resourcesUrl = '';

    http: HttpClient;
    authService: AuthService;
    alertService: AlertService;
    accountService: AccountService;
    configurations: ConfigurationService;
    appTitle: AppTitleService;

    toolTips: Array<any> = [];

    imageDefault = AppConsts.imageDefault;

    isDropZoneActive = false;
    imageSource = "";
    textVisible = true;
    progressVisible = false;
    progressValue = 0;

    isLoading = false;


    constructor(injector: Injector) {
        this.http = injector.get(HttpClient);
        this.accountService = injector.get(AccountService);
        this.alertService = injector.get(AlertService);
        this.authService = injector.get(AuthService);
        this.configurations = injector.get(ConfigurationService);
        this.appTitle = injector.get(AppTitleService);
        this.resourcesUrl = this.configurations.baseUrl + '/uploads';
    }

    toggleTooltip(id: any, mode: boolean) {
        if (this.toolTips.indexOf(id) >= 0) {
            this.toolTips[id] != this.toolTips[id];
        } else {
            this.toolTips[id] = mode;
        }
    }

    onUploaded(e: any) {
        console.log(e.request.response);
        // const file = e.file;
        // const fileReader = new FileReader();
        // fileReader.onload = () => {
        //     this.isDropZoneActive = false;
        //     this.imageSource = fileReader.result as string;
        // }
        // fileReader.readAsDataURL(file);
        // this.textVisible = false;
        // this.progressVisible = false;
        // this.progressValue = 0;
    }

    onProgress(e: any) {
        this.progressValue = e.bytesLoaded / e.bytesTotal * 100;
    }

    onUploadStarted(...args: any[]) {
        this.imageSource = "";
        this.progressVisible = true;
    }

    isGrant(permission: string): boolean {
        return this.accountService.userHasPermission(permission);
    }
}

export abstract class AbstractListComponent extends AbstractComponent {

    taskForm: any;

    dataSource: any = {};
    dataSourceTotalCount = 0;
    dataSourceGrid!: DxDataGridComponent;

    selectedRows = [];
    obj: any = {};
    popupVisible: boolean = false;

    toolTips: any = [];

    queryParams = ['skip', 'take', 'requireTotalCount', 'requireGroupCount', 'sort', 'filter', 'totalSummary', 'group', 'groupSummary'];

    pageName: string = '';

    libs = { name: 'titleCase' };

    ayncValidate(o: any): Promise<any> {
        return new Promise((resolve, reject) => {
            // @ts-ignore
            this.http.get(this.configurations.baseUrl + '/api/validation', { params: Object.assign(this.taskForm.formData?.id ? { id: this.taskForm.formData.id } : {}, { field: o.formItem.dataField, value: o.value, table: o.formItem.editorOptions.table }) })
                .toPromise()
                .then((resp: any) => resolve(resp))
                .catch((error) => reject(error));
        });
    }

    async onReorder(...args: any[]) { }

    async onRowEdit(cell: any = null) {
        this.popupVisible = !this.popupVisible;
        this.obj = {};
        if (this.taskForm) this.taskForm.instance.resetValues();

        if (this.taskForm && (cell && cell.row && cell.row.data)) this.taskForm.formData = this.obj = clone(cell.row.data);
    }

    async onSave(e: any) {
        e.preventDefault();
        if (this.taskForm) console.log(this.taskForm);
    }

    async onSelectionChanged(data: any) {
        this.selectedRows = data.selectedRowKeys;
    }

    async onRowDelete(e: any) {
        custom({
            messageHtml: 'Are you sure you want to delete this record ?',
            title: 'Confirm',
            buttons: [
                { text: 'Yes, delete it!', stylingMode: 'contained', type: 'default', onClick: (e) => true },
                { text: 'Cancel', type: 'normal', onClick: (e) => false }
            ]
        }).show().then(async (isSubmit: boolean) => {
            if (isSubmit) {
                await this.dataSource.remove(e.key);
                await e.component.refresh();
            }
        });
    }

    async onClosePopup() {
        this.popupVisible = false;
    }

    async onToolbarPreparing(e: any) {
        e.toolbarOptions.items.unshift(
            {
                location: 'after',
                widget: 'dxButton',
                options: {
                    icon: 'refresh',
                    stylingMode: 'contained',
                    onClick: this.onClearFiler = this.onClearFiler.bind(this)
                }
            },
            {
                location: 'after',
                widget: 'dxButton',
                options: {
                    icon: 'add',
                    type: 'default', stylingMode: 'contained',
                    onClick: this.onRowEdit = this.onRowEdit.bind(this)
                }
            });
    }

    async onHidePopup() {
        if (this.taskForm) this.taskForm.instance.resetValues();
        this.obj = {};
        this.selectedRows = [];
        // if (this.taskForm) this.taskForm.instance.resetValues();
    }

    async onClearFiler() {
        if (this.dataSourceGrid) this.dataSourceGrid.instance.clearFilter();
    }

    async onUploaded(e: any) {
        let resp: Array<any> = JSON.parse(e.request.response);
        this.imageSource = resp[0]?.url;
        this.textVisible = false;
        this.progressVisible = false;
        this.progressValue = 0;
    }

    async toggleTooltip(id: any, mode: boolean = false) {
        if (this.toolTips.indexOf(id) >= 0) {
            this.toolTips[id] != this.toolTips[id];
        } else {
            this.toolTips[id] = mode;
        }

        console.log(this.toolTips);
    }

    isNotEmpty(value: any): boolean {
        return value !== undefined && value !== null && value !== "";
    }

    async processReorder(e: any, key = 'key', field = 'weight') {
        await this.dataSource.update('bulkUpdate', Utilities.reOrderUpdateArray(e.component.getVisibleRows(), e.fromIndex, e.toIndex, key, field));
        await e.component.refresh();
    }

    public calculateCellValue(...args: any[]) {
        return '';
    }

    public changesSavedCallback: (() => void) | undefined;
    public changesFailedCallback: (() => void) | undefined;
    public changesCancelledCallback: (() => void) | undefined;

    public saveFailedHelper(error: any) {
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage('Save Error', 'The below errors occured whilst saving your changes:', MessageSeverity.error, error);
        this.alertService.showStickyMessage(error, '', MessageSeverity.error);

        if (this.changesFailedCallback) {
            this.changesFailedCallback();
        }
    }

    public refreshLoggedInUser() {
        this.accountService.refreshLoggedInUser()
            .subscribe((user: User) => { },
                (error: any) => {
                    this.alertService.resetStickyMessage();
                    this.alertService.showStickyMessage('Refresh failed', 'An error occured whilst refreshing logged in user information from the server', MessageSeverity.error, error);
                });
    }

    
}
