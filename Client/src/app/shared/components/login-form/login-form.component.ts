import { AfterViewInit, Component, OnInit } from '@angular/core';
import { AlertService, DialogType, MessageSeverity } from 'src/app/services/alert.service';

import { AuthService } from 'src/app/services/auth.service';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { UserLogin } from 'src/app/models/user-login.model';
import { Utilities } from 'src/app/services/utilities';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit, AfterViewInit {

  public id = 'login';

  isLoading = false;
  formData = new UserLogin();
  loginStatusSubscription: any;
  formResetToggle = true;

  notify: { visible: boolean, type: string, message?: any } = { type: 'info', visible: false };

  constructor(private alertService: AlertService, private authService: AuthService, private configurations: ConfigurationService) { }

  ngAfterViewInit(): void {

  }

  ngOnInit(): void {

    this.formData.rememberMe = this.authService.rememberMe;

    if (this.getShouldRedirect()) {
      this.authService.redirectLoginUser();
    } else {
      this.loginStatusSubscription = this.authService.getLoginStatusEvent().subscribe(isLoggedIn => {
        if (this.getShouldRedirect()) {
          this.authService.redirectLoginUser();
        }
      });
    }
  }

  ngOnDestroy() {
    if (this.loginStatusSubscription) {
      this.loginStatusSubscription.unsubscribe();
    }
  }

  getShouldRedirect() {
    return this.authService.isLoggedIn && !this.authService.isSessionExpired;
  }

  showErrorAlert(caption: string, message: string) {
    this.alertService.showMessage(caption, message, MessageSeverity.error);
  }

  async onSubmit(e: any) {
    e.preventDefault();

    this.isLoading = true;

    this.authService.loginWithPassword(this.formData.userName, this.formData.password, this.formData.rememberMe)
      .subscribe(
        user => {
          setTimeout(() => {
            this.alertService.stopLoadingMessage();
            this.isLoading = false;
            this.reset();

            this.alertService.showMessage('Login', `Session for ${user.userName} restored!`, MessageSeverity.success);

            setTimeout(() => {
              this.alertService.showStickyMessage('Session Restored', 'Please try your last operation again', MessageSeverity.default);
            }, 500);


          }, 500);
        },
        error => {

          this.alertService.stopLoadingMessage();

          if (Utilities.checkNoNetwork(error)) {
            this.alertService.showStickyMessage(Utilities.noNetworkMessageCaption, Utilities.noNetworkMessageDetail, MessageSeverity.error, error);
            this.offerAlternateHost();
          } else {
            const errorMessage = Utilities.getHttpResponseMessage(error);

            if (errorMessage) {
              this.alertService.showStickyMessage(this.mapLoginErrorMessage(errorMessage), 'Unable to login', MessageSeverity.error, error);
            } else {
              this.alertService.showStickyMessage('An error occured whilst logging in, please try again later.\nError: ' + Utilities.getResponseBody(error), 'Unable to login', MessageSeverity.error, error);
            }
          }

          setTimeout(() => {
            this.isLoading = false;
          }, 500);
        });
  }

  offerAlternateHost() {

    if (Utilities.checkIsLocalHost(location.origin) && Utilities.checkIsLocalHost(this.configurations.baseUrl)) {
      this.alertService.showDialog('Dear Developer!\nIt appears your backend Web API service is not running...\n' +
        'Would you want to temporarily switch to the online Demo API below?(Or specify another)',
        DialogType.prompt,
        (value: string) => {
          this.configurations.baseUrl = value;
          this.configurations.tokenUrl = value;
          this.alertService.showStickyMessage('API Changed!', 'The target Web API has been changed to: ' + value, MessageSeverity.warn);
        },
        // @ts-ignore
        null,
        null,
        null,
        this.configurations.fallbackBaseUrl);
    }
  }


  mapLoginErrorMessage(error: string) {

    if (error === 'invalid_username_or_password') {
      return 'Invalid username or password';
    }

    if (error === 'invalid_grant') {
      return 'This account has been disabled';
    }

    return error;
  }

  reset() {
    this.formResetToggle = false;

    setTimeout(() => {
      this.formResetToggle = true;
    });
  }
}