import { DxButtonModule, DxCheckBoxModule, DxContextMenuModule, DxDataGridModule, DxDrawerModule, DxDropDownBoxModule, DxDropDownButtonModule, DxFileManagerModule, DxFileUploaderModule, DxFormModule, DxListModule, DxLoadIndicatorModule, DxMenuModule, DxMultiViewModule, DxPopupModule, DxProgressBarModule, DxRadioGroupModule, DxResponsiveBoxModule, DxScrollViewModule, DxSwitchModule, DxTagBoxModule, DxTemplateModule, DxTextAreaModule, DxToastModule, DxToolbarModule, DxTooltipModule, DxTreeViewModule, DxValidationSummaryModule, DxValidatorModule } from 'devextreme-angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CommonModule } from '@angular/common';
import { DebugPipe } from '../pipes/debug.pipe';
import { DxoPositionModule } from 'devextreme-angular/ui/nested';
import { GroupByPipe } from '../pipes/group-by.pipe';
import { ImagePreloadDirective } from '../directives';
import { JoinPipe } from '../pipes/join.pipe';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { MapKeyPipe } from '../pipes/map-key.pipe';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SafePipe } from '../pipes/safe.pipe';

@NgModule({
  declarations: [
    // Pipe
    SafePipe, GroupByPipe, MapKeyPipe, JoinPipe, DebugPipe,

    // Directive
    ImagePreloadDirective,

    // Component

  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,

    DxButtonModule,
    DxCheckBoxModule,
    DxContextMenuModule,
    DxDataGridModule,
    DxDrawerModule,
    DxDropDownBoxModule,
    DxDropDownButtonModule,
    DxFileManagerModule,
    DxFileUploaderModule,
    DxFormModule,
    DxListModule,
    DxLoadIndicatorModule,
    DxMenuModule,
    DxMultiViewModule,
    DxoPositionModule,
    DxPopupModule,
    DxProgressBarModule,
    DxRadioGroupModule,
    DxResponsiveBoxModule,
    DxScrollViewModule,
    DxSwitchModule,
    DxTagBoxModule,
    DxTemplateModule,
    DxTextAreaModule,
    DxToastModule,
    DxToolbarModule,
    DxTooltipModule,
    DxTreeViewModule,
    DxValidationSummaryModule,
    DxValidatorModule,

  ],
  exports: [
    // Pipe
    SafePipe, GroupByPipe, JoinPipe, MapKeyPipe, DebugPipe,

    // Directive
    ImagePreloadDirective,

    // Component
    RouterModule,

    DxButtonModule,
    DxCheckBoxModule,
    DxContextMenuModule,
    DxDataGridModule,
    DxDrawerModule,
    DxDropDownBoxModule,
    DxDropDownButtonModule,
    DxFileManagerModule,
    DxFileUploaderModule,
    DxFormModule,
    DxListModule,
    DxLoadIndicatorModule,
    DxMenuModule,
    DxMultiViewModule,
    DxoPositionModule,
    DxPopupModule,
    DxProgressBarModule,
    DxRadioGroupModule,
    DxResponsiveBoxModule,
    DxScrollViewModule,
    DxSwitchModule,
    DxTagBoxModule,
    DxTemplateModule,
    DxTextAreaModule,
    DxToastModule,
    DxToolbarModule,
    DxTooltipModule,
    DxTreeViewModule,
    DxValidationSummaryModule,
    DxValidatorModule,
  ],
  entryComponents: [LoginFormComponent]
})
export class SharedModule {
}
