import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ConfigurationService } from './configuration.service';
import { EndpointBase } from './endpoint-base.service';
import { AlertService } from './alert.service';
import { AuthService } from './auth.service';
import { map } from "rxjs/operators";
import { SinhVien1 } from '../models/sinhvien.model';
@Injectable({
  providedIn: 'root'
})
export class SinhvienendpointService extends EndpointBase {
  get coursesUrl() { return this.configurations.baseUrl + '/api/course'; }
  empurl = 'http://localhost:11568/api/TTSinhVien';

  constructor(private configurations: ConfigurationService,protected http: HttpClient, authService: AuthService, alertService: AlertService) {
    super(http,authService,alertService);
   }
   postsinhvienEndpoint<T>(obj: any) {
    console.log(JSON.stringify(obj)+"Mới toanh")
    const url =`http://localhost:11568/api/ttsinhvien`;
    return this.http.post<T>(url, JSON.stringify(obj), this.requestHeaders).pipe(map(data => {}));
    }
  //  postsinhvienEndpoint <T>(params: any) {
      // console.log("Id của bạn " +params);
    //   const url =`http://localhost:11568/api/TTSinhVien`;
    //   var { headers } = this.requestHeaders;
    //   return this.http.get<T>(url, { headers, params: params })
    // }
  Delete(id : string){
      console.log("Id của bạn " +id);
      const endpointUrl = `${this.empurl}/${id}`;
      return this.http.delete(endpointUrl, this.requestHeaders).pipe(map(data => {}));
    }
}
