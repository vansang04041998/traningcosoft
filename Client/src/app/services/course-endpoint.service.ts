// @ts-nocheck

import { AlertService } from './alert.service';
import { AuthService } from './auth.service';
import { ConfigurationService } from './configuration.service';
import { EndpointBase } from './endpoint-base.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class CourseEndpoint extends EndpointBase {



    get coursesUrl() { return this.configurations.baseUrl + '/api/course'; }
    get genrsesUrl() { return this.configurations.baseUrl + '/api/genre'; }
    get classRoomsUrl() { return this.configurations.baseUrl + '/api/classRooms'; }
    get ordersUrl() { return this.configurations.baseUrl + '/api/order'; }

    constructor(private configurations: ConfigurationService, http: HttpClient, authService: AuthService, alertService: AlertService) {
        super(http, authService, alertService);
    }

    getCoursesAllEndpoint<T>(params?: any): Observable<T> {
        const endpointUrl = this.coursesUrl;
        var { headers } = this.requestHeaders;
        return this.http.get<T>(endpointUrl, { headers, params: params }).pipe<T>(
            catchError(error => {
                return this.handleError(error, () => this.getCoursesAllEndpoint(params));
            }));
    }

    getClassRoomsAllEndpoint<T>(params: any) {
        const endpointUrl = this.classRoomsUrl;
        var { headers } = this.requestHeaders;
        return this.http.get<T>(endpointUrl, { headers, params: params }).pipe<T>(
            catchError(error => {
                return this.handleError(error, () => this.getClassRoomsAllEndpoint(params));
            }));
    }

    getGenresAllEndpoint<T>(params?: any): Observable<T> {
        const endpointUrl = this.genrsesUrl;
        var { headers } = this.requestHeaders;
        return this.http.get<T>(endpointUrl, { headers, params: params }).pipe<T>(
            catchError(error => {
                return this.handleError(error, () => this.getGenresAllEndpoint(params));
            }));
    }

    getOrdersAllEndpoint<T>(params: any) {
        const endpointUrl = this.ordersUrl;
        var { headers } = this.requestHeaders;
        return this.http.get<T>(endpointUrl, { headers, params: params }).pipe<T>(
            catchError(error => {
                return this.handleError(error, () => this.getOrdersAllEndpoint(params));
            }));
    }

    getNewGenreEndpoint<T>(obj: any) {
        return this.http.post<T>(this.genrsesUrl, JSON.stringify(obj), this.requestHeaders).pipe<T>(
            catchError(error => {
                return this.handleError(error, () => this.getNewGenreEndpoint(obj));
            }));
    }

    getUpdateGenreEndpoint(obj: any, id: any) {
        const endpointUrl = `${this.genrsesUrl}/${id}`;

        return this.http.put<T>(endpointUrl, JSON.stringify(obj), this.requestHeaders).pipe<T>(
            catchError(error => {
                return this.handleError(error, () => this.getUpdateGenreEndpoint(obj, id));
            }));
    }

    getDeleteGenreEndpoint(genreId: string) {
        const endpointUrl = `${this.genrsesUrl}/${genreId}`;

        return this.http.delete<T>(endpointUrl, this.requestHeaders).pipe<T>(
            catchError(error => {
                return this.handleError(error, () => this.getDeleteGenreEndpoint(genreId));
            }));
    }


    getNewCourseEndpoint<T>(obj: any) {
        return this.http.post<T>(this.coursesUrl, JSON.stringify(obj), this.requestHeaders).pipe<T>(
            catchError(error => {
                return this.handleError(error, () => this.getNewCourseEndpoint(obj));
            }));
    }

    getUpdateCourseEndpoint(obj: any, id: any) {
        const endpointUrl = `${this.coursesUrl}/${id}`;

        return this.http.put<T>(endpointUrl, JSON.stringify(obj), this.requestHeaders).pipe<T>(
            catchError(error => {
                return this.handleError(error, () => this.getUpdateCourseEndpoint(obj, id));
            }));
    }

    getDeleteCourseEndpoint(courseId: string) {
        const endpointUrl = `${this.coursesUrl}/${courseId}`;

        return this.http.delete<T>(endpointUrl, this.requestHeaders).pipe<T>(
            catchError(error => {
                return this.handleError(error, () => this.getDeleteCourseEndpoint(courseId));
            }));
    }

    getNewClassRoomEndpoint<T>(obj: any) {
        return this.http.post<T>(this.classRoomsUrl, JSON.stringify(obj), this.requestHeaders).pipe<T>(
            catchError(error => {
                return this.handleError(error, () => this.getNewClassRoomEndpoint(obj));
            }));
    }

    getUpdateClassRoomEndpoint(obj: any, id: any) {
        const endpointUrl = `${this.classRoomsUrl}/${id}`;

        return this.http.put<T>(endpointUrl, JSON.stringify(obj), this.requestHeaders).pipe<T>(
            catchError(error => {
                return this.handleError(error, () => this.getUpdateClassRoomEndpoint(obj, id));
            }));
    }

    getDeleteClassRoomEndpoint(classRoomId: string) {
        const endpointUrl = `${this.classRoomsUrl}/${classRoomId}`;

        return this.http.delete<T>(endpointUrl, this.requestHeaders).pipe<T>(
            catchError(error => {
                return this.handleError(error, () => this.getDeleteClassRoomEndpoint(classRoomId));
            }));
    }

    getOneClassRoomEndpoint(id: string | undefined) {
        const endpointUrl = `${this.classRoomsUrl}/${id}`;

        return this.http.get<T>(endpointUrl, this.requestHeaders).pipe<T>(
            catchError(error => {
                return this.handleError(error, () => this.getOneClassRoomEndpoint(id));
            }));
    }

    getNewOrderEndpoint<T>(obj: any) {
        return this.http.post<T>(this.ordersUrl, JSON.stringify(obj), this.requestHeaders).pipe<T>(
            catchError(error => {
                return this.handleError(error, () => this.getNewOrderEndpoint(obj));
            }));
    }

    getUpdateOrderEndpoint(obj: any, id: any) {
        const endpointUrl = `${this.ordersUrl}/${id}`;

        return this.http.put<T>(endpointUrl, JSON.stringify(obj), this.requestHeaders).pipe<T>(
            catchError(error => {
                return this.handleError(error, () => this.getUpdateOrderEndpoint(obj, id));
            }));
    }

    getDeleteOrderEndpoint(orderId: string) {
        const endpointUrl = `${this.ordersUrl}/${orderId}`;

        return this.http.delete<T>(endpointUrl, this.requestHeaders).pipe<T>(
            catchError(error => {
                return this.handleError(error, () => this.getDeleteOrderEndpoint(orderId));
            }));
    }
}
