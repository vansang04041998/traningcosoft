import { AuthService } from './auth.service';
import { CourseEndpoint } from './course-endpoint.service';
import { Injectable } from '@angular/core';

@Injectable()
export class CourseService {

    constructor(
        private authService: AuthService,
        private courseEndpoint: CourseEndpoint) {

    }

    getAllCourses(params?: any) {
        return this.courseEndpoint.getCoursesAllEndpoint<any>(params);
    }

    getAllGenres(params?: any) {
        return this.courseEndpoint.getGenresAllEndpoint<any>(params);
    }

    getAllClassRooms(params?: any) {
        return this.courseEndpoint.getClassRoomsAllEndpoint<any>(params);
    }

    getAllOrders(params?: any) {
        return this.courseEndpoint.getOrdersAllEndpoint<any>(params);
    }

    newGenre(obj: any) {
        return this.courseEndpoint.getNewGenreEndpoint<any>(obj);
    }

    updateGenre(obj: any) {
        return this.courseEndpoint.getUpdateGenreEndpoint(obj, obj.id);
    }

    deleteGenre(genreId: string) {
        return this.courseEndpoint.getDeleteGenreEndpoint(genreId);
    }

    newCourse(obj: any) {
        return this.courseEndpoint.getNewCourseEndpoint<any>(obj);
    }

    updateCourse(obj: any) {
        return this.courseEndpoint.getUpdateCourseEndpoint(obj, obj.id);
    }

    deleteCourse(courseId: string) {
        return this.courseEndpoint.getDeleteCourseEndpoint(courseId);
    }

    newClassRoom(obj: any) {
        return this.courseEndpoint.getNewClassRoomEndpoint<any>(obj);
    }

    updateClassRoom(obj: any) {
        return this.courseEndpoint.getUpdateClassRoomEndpoint(obj, obj.id);
    }

    deleteClassRoom(ClassRoomId: string) {
        return this.courseEndpoint.getDeleteClassRoomEndpoint(ClassRoomId);
    }

    newOrder(obj: any) {
        return this.courseEndpoint.getNewOrderEndpoint<any>(obj);
    }

    updateOrder(obj: any) {
        return this.courseEndpoint.getUpdateOrderEndpoint(obj, obj.id);
    }

    deleteOrder(orderId: string) {
        return this.courseEndpoint.getDeleteOrderEndpoint(orderId);
    }

    getOneClassRoom(id?: string) {
        return this.courseEndpoint.getOneClassRoomEndpoint(id);
    }
    // updateCourse(course: CourseEdit) {
    //     if (course.id) {
    //         return this.courseEndpoint.getUpdateCourseEndpoint(course, course.id);
    //     } else {
    //         return this.courseEndpoint.getCourseByCourseNameEndpoint<Course>(course.courseName as any).pipe(
    //             mergeMap(foundCourse => {
    //                 course.id = foundCourse.id;
    //                 return this.courseEndpoint.getUpdateCourseEndpoint(course, course.id);
    //             }));
    //     }
    // }

    // newCourse(course: CourseEdit) {
    //     return this.courseEndpoint.getNewCourseEndpoint<Course>(course);
    // }


    // deleteCourse(courseOrCourseId: string | Course): Observable<Course> {
    //     if (typeof courseOrCourseId === 'string' || courseOrCourseId instanceof String) {
    //         return this.courseEndpoint.getDeleteCourseEndpoint<Course>(courseOrCourseId as string).pipe<Course>(
    //             tap(data => this.onRolesCourseCountChanged(data.roles as any)));
    //     } else {
    //         if (courseOrCourseId.id) {
    //             return this.deleteCourse(courseOrCourseId.id);
    //         } else {
    //             return this.courseEndpoint.getCourseByCourseNameEndpoint<Course>(courseOrCourseId.courseName as any).pipe<Course>(
    //                 mergeMap(course => this.deleteCourse(course.id as any)));
    //         }
    //     }
    // } 
}
