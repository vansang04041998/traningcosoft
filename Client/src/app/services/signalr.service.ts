import * as signalR from '@aspnet/signalr';

import { Observable, Subject } from 'rxjs';

import { AccountService } from './account.service';
import { AlertService } from './alert.service';
import { AuthService } from './auth.service';
import { ConfigurationService } from './configuration.service';
import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { Utilities } from './utilities';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class SignalRService {

    private _notify$: Subject<any>;
    private _updateRole$: Subject<any>;
    private _message$: Subject<any>;
    private _device$: Subject<any>;
    private _connection: signalR.HubConnection;

    constructor(private configurations: ConfigurationService, private authService: AuthService, private accountService: AccountService, alertService: AlertService) {
        this._notify$ = new Subject<any>();
        this._updateRole$ = new Subject<any>();
        this._message$ = new Subject<any>();
        this._device$ = new Subject<any>();
        this._connection = new signalR.HubConnectionBuilder()
            .withUrl((environment.baseUrl || Utilities.baseUrl()) + '/channel', { accessTokenFactory: () => this.authService.accessToken })
            .build();

        // this.authService.getLoginStatusEvent().subscribe(isLoggedIn => {
        //     if (isLoggedIn) {
        //         this.connect();
        //         console.log('-------------------------------------------------- connected');
        //     } else {
        //         console.log('stop for logout --------------------------------------------------');
        //         this._connection.stop();
        //     }
        // });
    }

    // tslint:disable-next-line: naming-convention
    public connect() {
        this._connection.start().catch((error) => {
            if (error) {
                if (error.message) {
                    console.error(error.message);
                }
                if (error.statusCode && error.statusCode === 401) {
                    this.accountService.refreshLoggedInUser().subscribe((user: User) => {
                        this._connection.start();
                    });
                }
            }
        });

        this._connection.on('SystemNotification', (message) => {
            // toastr.info(message, { positionClass: 'top-center', preventDuplicates: true, preventOpenDuplicates: true });
            this._notify$.next(message);
        });

        this._connection.on('DeviceUpdate', (message) => {
            this._device$.next(JSON.parse(message));
        });

        this._connection.on('Message', (message) => {
            this._message$.next(message);
        });

        this._connection.on('AdminUpdateRole', (rolName) => {
            if (this.accountService.currentUser.roles?.some(r => r === rolName)) {
                this.accountService.refreshLoggedInUser().subscribe((user: User) => { this._updateRole$.next(rolName); }, (error: any) => { console.log(error); });
            }
        });
    }

    public onNotify(): Observable<any> {
        return this._notify$;
    }

    public onUpdateRole(): Observable<any> {
        return this._updateRole$;
    }

    public onMessage(): Observable<any> {
        return this._message$;
    }

    public getDevice(): Observable<any> {
        return this._device$;
    }

    public disconnect() {
        this._connection.stop();
    }
}
