import { animate, query, state, style, transition, trigger } from '@angular/animations';

export const fadeInOut = trigger('fadeInOut', [
  transition(':enter', [style({ opacity: 0 }), animate('1s ease-in', style({ opacity: 1 }))]),
  transition(':leave', [animate('0.4s 10ms ease-out', style({ opacity: 0 }))])
]);

export function flyInOut(duration: number = 0.2) {
  return trigger('flyInOut', [
    state('in', style({ opacity: 1, transform: 'translateX(0)' })),
    transition('void => *', [style({ opacity: 0, transform: 'translateX(-100%)' }), animate(`${duration}s ease-in`)]),
    transition('* => void', [animate(`${duration}s 10ms ease-out`, style({ opacity: 0, transform: 'translateX(100%)' }))])
  ]);
}


export const fadeAnimation = trigger('fadeAnimation', [
  transition('* => *', [
    query(':enter',
      [
        style({ opacity: 0 })
      ],
      { optional: true }
    ),
    query(':leave',
      [
        style({ opacity: 1 }),
        animate('.2s', style({ opacity: 0 }))
      ],
      { optional: true }
    ),
    query(':enter',
      [
        style({ opacity: 0 }),
        animate('.2s', style({ opacity: 1 }))
      ],
      { optional: true }
    )
  ])
]);