import { BehaviorSubject, from } from 'rxjs';
import { ComponentFactoryResolver, Injectable, ViewContainerRef } from '@angular/core';

import { map } from 'rxjs/operators';

export interface ComponentLoader {
    loadChildren: () => Promise<any>;
}

@Injectable({
    providedIn: 'root'
})
export class AppService {
    constructor(private cfr: ComponentFactoryResolver) { }

    forChild(vcr: ViewContainerRef, cl: ComponentLoader) {
        return from(cl.loadChildren()).pipe(
            map((component: any) => this.cfr.resolveComponentFactory(component)),
            map(componentFactory => vcr.createComponent(componentFactory))
        );
    }
}


@Injectable({ providedIn: 'root' })
export class ComponentService {
    private isLoggedIn = new BehaviorSubject(false);
    isLoggedIn$ = this.isLoggedIn.asObservable();

    constructor(private appService: AppService) { }

    private Login() {
        return () => import('../shared/components/login-form/login-form.component').then(c => c.LoginFormComponent);
    }

    // private clientProfile() {
    //     return () =>
    //         import('./client-profile/client-profile.component').then(
    //             m => m.ClientProfileComponent
    //         );
    // }

    loadComponent(vcr: ViewContainerRef, isLoggedIn: boolean = false) {
        vcr.clear();
        return this.appService.forChild(vcr, {
            loadChildren: this.Login()
        });
    }
}