;
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SinhvienendpointService } from './sinhvienendpoint.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SinhVien, SinhVien1 } from '../models/sinhvien.model';
import { catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class SinhvienserviceService {
  Dulieu: SinhVien[] = [];
  empurl = 'http://localhost:11568/api/TTSinhVien';
      private httpOptions={
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      };
  constructor(private http :HttpClient, private sinhvienendpoint: SinhvienendpointService ) {
   }
   ///Lấy dữ liệu
   public getdata(): Observable <any>
   {
     const url =`http://localhost:11568/api/TTSinhVien`;
     return this.http.get<any>(url, this.httpOptions);
   }
   postnew(obj? :any) {
    return this.sinhvienendpoint.postsinhvienEndpoint<any>(obj);
  }
  public delete(id: any) {
    return this.sinhvienendpoint.Delete(id);
  }
  
}
