export const environment = {
  production: true,
  baseUrl: 'http://103.226.251.46:5000', // Change this to the address of your backend API if different from frontend address
  tokenUrl: 'http://103.226.251.46:5000', // For IdentityServer/Authorization Server API. You can set to null if same as baseUrl
  loginUrl: '/login'
};
