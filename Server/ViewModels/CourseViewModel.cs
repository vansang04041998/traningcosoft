﻿using FluentValidation;

using Microsoft.EntityFrameworkCore;

using SHAI.Models;

using System;
using System.Collections.Generic;
using System.Linq;

namespace SHAI.ViewModels
{
    public class CourseBaseViewModel : AuditableEntity
    {
        public string Id { get; set; }
        public string GenreId { get; set; } 
        public string Name { get; set; }
        public string ChineseName { get; set; }
        public string Image { get; set; }
        public string Code { get; set; }
        public string Requirements { get; set; }
        public string Language { get; set; }
        public int QuotaPerLesson { get; set; }
        public string Description { get; set; }

        public int NumberOfLesson { get; set; }

        public virtual ICollection<ClassRoom> ClassRooms { get; set; }
        public virtual Genre Genre { get; set; }
    }

    public class CourseViewModel : CourseBaseViewModel
    {
        public CourseViewModel() : base() { }

        public CourseViewModel(Course o, ApplicationDbContext _context)
        {
            Id = o.Id;
            Name = o.Name;
            ChineseName = o.ChineseName;
            Image = o.Image;
            Code = o.Code;
            Requirements = o.Requirements;
            Language = o.Language;
            QuotaPerLesson = o.QuotaPerLesson;
            NumberOfLesson = o.NumberOfLesson;
            Description = o.Description;
            GenreId = o.GenreId;
            Genre = _context.Genres.FromSqlRaw(@"SELECT * FROM dbo.Genres WHERE Genres.Id = {0}", o.GenreId).FirstOrDefault();

            CreatedBy = o.CreatedBy;
            UpdatedBy = o.UpdatedBy;
            CreatedDate = o.CreatedDate;
            UpdatedDate = o.UpdatedDate;
        }
    }

    public class CourseEditViewModel : CourseBaseViewModel
    {
        public CourseEditViewModel() : base() { } 
    }

    public class CourseViewModelValidator : AbstractValidator<CourseViewModel>
    {
        public CourseViewModelValidator()
        {
            RuleFor(register => register.Name).NotEmpty().WithMessage("Course name cannot be empty");
        }
    }
}
