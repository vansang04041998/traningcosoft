﻿using Microsoft.EntityFrameworkCore;

using SHAI.Models;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SHAI.ViewModels
{
    public class GenreBaseViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Weight { get; set; }
    }

    public class GenreViewModel : GenreBaseViewModel
    {
        public GenreViewModel() : base() { }

        public GenreViewModel(Genre g, ApplicationDbContext _context)
        {
            var _courses = _context.Courses.FromSqlRaw(@"SELECT *
                                                    FROM dbo.Courses
                                                    WHERE Courses.GenreId = {0}", g.Id).Select(u => new UserGenreViewModel(u)).ToArray();

            Id = g.Id;
            Name = g.Name;
            Description = g.Description;
            Weight = g.Weight;
            Courses = _courses;
            CourseCount = _courses.Length;
        }

        public int CourseCount { get; set; } = 0;

        public ICollection<UserGenreViewModel> Courses { get; set; }
    }

    public class UserGenreViewModel
    {
        public UserGenreViewModel(Course g)
        {
            Id = g.Id;
            Name = g.Name;
            NumberOfLesson = g.NumberOfLesson;
        }

        //public string[] Users { get; set; }
        public string Id {  get; set; }
        public string Name {  get; set; }
        public int NumberOfLesson { get; set; }
    }

    public class GenreEditViewModel : GenreBaseViewModel
    {
        //public string[] Users { get; set; }
    }
}
