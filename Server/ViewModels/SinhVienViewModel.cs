﻿using SHAI.Helpers;
using FluentValidation;
using System.ComponentModel.DataAnnotations;
using SHAI.Models;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
namespace SHAI.ViewModels
{
    public class SinhVienViewModel : SinhVienBaseViewModel
    {
        public SinhVienViewModel() : base() { }

        //public SinhVienViewModel(SinhVien u, string[] roles, ApplicationDbContext _context)
        //{
        //    var sinhvien = _context.SinhViens.FromSqlRaw(@"SELECT SinhViens.*
        //                                    FROM dbo.SinhVien", u.HoTen).Select(g => g.HoTen).ToArray();
        //    HoTen = u.HoTen;
        //    Ngaysinh = u.Ngaysinh;
        //    GioiTinh = u.GioiTinh;
        //    SoThich = u.SoThich;
        //    HocLuc = u.HocLuc;
        //    SDT = u.SDT;
        //}
        public SinhVienViewModel(SinhVien o)
        {
            Id = o.Id;
            HoTen = o.HoTen;
            Ngaysinh = o.Ngaysinh;
            GioiTinh = o.GioiTinh;
            SoThich = o.SoThich;
            HocLuc = o.HocLuc;
            SDT = o.SDT;
        }

    }

    public abstract class SinhVienBaseViewModel
    {
        public string Id { get; set; }
        public string HoTen { get; set; }
        public DateTime Ngaysinh { get; set; }
        public int? GioiTinh { get; set; }
        public string SoThich { get; set; }
        public string HocLuc { get; set; }
        public int? SDT { get; set; }
    }

}
