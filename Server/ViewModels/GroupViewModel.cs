﻿using Microsoft.EntityFrameworkCore;

using SHAI.Models;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SHAI.ViewModels
{
    public class GroupBaseViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Weight { get; set; }
    }

    public class GroupViewModel : GroupBaseViewModel
    {
        public GroupViewModel() : base() { }

        public GroupViewModel(Group g, ApplicationDbContext _context)
        {
            Id = g.Id;
            Name = g.Name;
            Description = g.Description;
            Weight = g.Weight;
            Users = _context.Users.FromSqlRaw(@"SELECT Users.*
                                                FROM dbo.UserGroups
                                                INNER JOIN dbo.Users ON UserGroups.UsersId = Users.Id
                                                WHERE UserGroups.GroupsId = {0}", g.Id).Select(u => new UserGroupViewModel(u)).ToArray();
        }

        public ICollection<UserGroupViewModel> Users { get; set; }
    }

    public class GroupEditViewModel : GroupBaseViewModel
    {
        public string[] Users { get; set; }
    }

    public class GroupEditMemberViewModel 
    {
        public string Member { get; set; }
        public string Type { get; set; }
    }
}
