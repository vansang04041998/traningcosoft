﻿using Microsoft.EntityFrameworkCore;

using SHAI.Models;

using System;
using System.Linq;

namespace SHAI.ViewModels
{
    public abstract class OrderBaseViewModel
    {
        public string Id { get; set; }
        public string ClassRoomId { get; set; }
        public string MemberId { get; set; }
        public int Status { get; set; }

        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class OrderViewModel : OrderBaseViewModel
    {
        public OrderViewModel() : base() { }

        public OrderViewModel(Order o, ApplicationDbContext _context)
        {
            Id = o.Id; 
            Member = _context.Users.FromSqlRaw(@"SELECT Users.*
                                                FROM dbo.Users
                                                INNER JOIN dbo.Orders ON Orders.MemberId = Users.Id
                                                WHERE Orders.Id = {0}", o.Id).Select(u => new UserGroupViewModel(u)).FirstOrDefault();
            ClassRoom = _context.ClassRooms.FromSqlRaw(@"SELECT ClassRooms.*
                                                FROM dbo.ClassRooms
                                                INNER JOIN dbo.Orders ON Orders.ClassRoomId = ClassRooms.Id
                                                WHERE Orders.Id = {0}", o.Id).Select(u => new ClassRoomViewModel(u, _context)).FirstOrDefault();
            CreatedDate = o.CreatedDate;
            UpdatedDate = o.UpdatedDate;
        }

        public UserGroupViewModel Member { get; set; }
        public ClassRoomViewModel ClassRoom { get; set; }
    }

    public class OrderEditViewModel : OrderBaseViewModel
    {
        
    }
}
