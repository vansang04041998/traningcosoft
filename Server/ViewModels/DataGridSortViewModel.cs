﻿namespace SHAI.ViewModels
{
    public class DataGridSortViewModel
    {
        public bool Desc { get; set; }
        public string Selector { get; set; }
    }

    public class StoreFilterViewModel
    {
        public enum FilterComparator
        {
            Equals,
            NotEquals,
            Contains,
            GreaterThan,
            GreaterThanOrEqual,
            LessThan,
            LessThanOrEqual
        }

        public FilterComparator Comparator { get; set; }
        public string Field { get; set; }
        public string Value { get; set; }
    }
}
