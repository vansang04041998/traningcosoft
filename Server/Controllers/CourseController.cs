﻿using AutoMapper;
using SHAI.Core.Interfaces;
using SHAI.Models;

using IdentityServer4.AccessTokenValidation;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SHAI.Helpers;
using SHAI.ViewModels;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DevExtreme.AspNet.Data;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Identity;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;

namespace SHAI.Controllers
{
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    public class CourseController : ControllerBase
    {
        private IHubContext<ServerHub> _hub;
        private readonly IMapper _mapper;
        private readonly IAccountManager _accountManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IAuthorizationService _authorizationService;
        private readonly ILogger<AccountController> _logger;

        private readonly ApplicationDbContext _context;

        public CourseController(ApplicationDbContext context,
           IMapper mapper,
           IAccountManager accountManager,
           UserManager<ApplicationUser> userManager,
           IAuthorizationService authorizationService,
           ILogger<AccountController> logger,
           IHubContext<ServerHub> hub
           )
        {
            _hub = hub;
            _mapper = mapper;
            _accountManager = accountManager;
            _userManager = userManager;
            _authorizationService = authorizationService;
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        [Authorize(Authorization.Policies.ViewAllCoursesPolicy)]
        [ProducesResponseType(200, Type = typeof(List<CourseViewModel>))]
        public IActionResult GetCourses(DataSourceLoadOptions loadOptions)
        {
            return Ok(DataSourceLoader.Load(_context.Courses.Select(o => new CourseViewModel(o, _context)).ToList(), loadOptions));
        }

        [HttpPost]
        [ProducesResponseType(201, Type = typeof(CourseViewModel))]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        public async Task<IActionResult> Post([FromBody] CourseEditViewModel obj)
        {
            //if (!(await _authorizationService.AuthorizeAsync(this.User, (user.Roles, new string[] { }), Authorization.Policies.AssignAllowedRolesPolicy)).Succeeded)
            //    return new ChallengeResult();

            if (ModelState.IsValid)
            {
                if (obj == null) return BadRequest($"{nameof(Course)} cannot be null");

                var num = _context.ScalarIntValue.FromSqlRaw(@"SELECT ISNULL(CAST((SELECT TOP 1 * FROM STRING_SPLIT((SELECT TOP 1 Id FROM dbo.Courses c ORDER BY Id DESC) , '-')  ORDER BY value ASC) AS INTEGER), 0) AS Value").FirstOrDefault().Value;

                obj.Id = Utilities.PaddInt(num + 1, 4, "SHAI-HY-");

                _context.Courses.Add(new Course()
                {
                    Id = obj.Id,
                    Name = obj.Name,
                    ChineseName = obj.ChineseName,
                    Requirements = obj.Requirements,
                    Language = obj.Language,
                    QuotaPerLesson = obj.QuotaPerLesson,
                    NumberOfLesson = obj.NumberOfLesson,
                    Description = obj.Description,
                    GenreId = obj.GenreId
                });

                await _context.SaveChangesAsync();

                return NoContent();
            }
            else
            {
                return BadRequest(ModelState);
            }

        }

        [HttpPut("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Update(string id, [FromBody] CourseBaseViewModel obj)
        {
            if (ModelState.IsValid)
            {
                if (obj == null) return BadRequest($"Course cannot be null");

                var course = await _context.Courses.FindAsync(id);
                if (course == null) return NotFound(id);

                course.Name = obj.Name;
                course.ChineseName = obj.ChineseName;
                course.Requirements = obj.Requirements;
                course.Language = obj.Language;
                course.QuotaPerLesson = obj.QuotaPerLesson;
                course.NumberOfLesson = obj.NumberOfLesson;
                course.Description = obj.Description;
                course.GenreId = obj.GenreId;

                course.UpdatedDate = DateTime.Now;

                _context.Courses.Update(course);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Delete(string id)
        {
            Course Course = await _context.Courses.FindAsync(id);

            if (Course == null) return NotFound(id);

            _context.Courses.Remove(Course);

            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
