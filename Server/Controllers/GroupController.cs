﻿using AutoMapper;

using SHAI.Core;
using SHAI.Core.Interfaces;
using SHAI.Models;

using IdentityServer4.AccessTokenValidation;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using SHAI.Authorization;
using SHAI.Helpers;
using SHAI.ViewModels;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DevExtreme.AspNet.Data;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Identity;
using System.Linq.Dynamic.Core;
using System.Data.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;

namespace SHAI.Controllers
{
    [Route("api/[controller]")]
    public class GroupController : ControllerBase
    {
        private IHubContext<ServerHub> _hub;
        private readonly IMapper _mapper;
        private readonly IAccountManager _accountManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IAuthorizationService _authorizationService;
        private readonly ILogger<AccountController> _logger;
        private readonly ApplicationDbContext _context;

        public GroupController(ApplicationDbContext context,
            IMapper mapper,
            IAccountManager accountManager,
            UserManager<ApplicationUser> userManager,
            IAuthorizationService authorizationService,
            ILogger<AccountController> logger,
            IHubContext<ServerHub> hub
            )
        {
            _hub = hub;
            _mapper = mapper;
            _accountManager = accountManager;
            _userManager = userManager;
            _authorizationService = authorizationService;
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var objs = _context.Groups.OrderBy("Name").Select(o => new GroupViewModel(o, _context));
            return Ok(new { Data = objs, TotalCount = objs.Count(), GroupCount = -1 });
        }

        [HttpGet("{loadOptions}")]
        public IActionResult GetAll(DataSourceLoadOptions loadOptions)
        {
            return Ok(DataSourceLoader.Load(_context.Groups.Select(o => new GroupViewModel(o, _context)).ToList(), loadOptions));
        }

        [HttpPost]
        [ProducesResponseType(201, Type = typeof(GroupViewModel))]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        public async Task<IActionResult> Post([FromBody] GroupEditViewModel group)
        {
            //if (!(await _authorizationService.AuthorizeAsync(this.User, (user.Roles, new string[] { }), Authorization.Policies.AssignAllowedRolesPolicy)).Succeeded)
            //    return new ChallengeResult();

            if (ModelState.IsValid)
            {
                if (group == null) return BadRequest($"{nameof(group)} cannot be null");

                group.Id = Guid.NewGuid().ToString();

                _context.Groups.Add(new Group() { Id = group.Id, Name = group.Name, Description = group.Description, Weight = group.Weight });

                await _context.SaveChangesAsync();

                return NoContent();
            }
            else
            {
                return BadRequest(ModelState);
            }

        }

        [HttpPut("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Update(string id, [FromBody] GroupEditViewModel groupEdit)
        {
            if (ModelState.IsValid)
            {
                if (groupEdit == null) return BadRequest($"{nameof(groupEdit)} cannot be null");

                var group = await _context.Groups.FindAsync(id);
                if (group == null) return NotFound(id);

                group.Name = groupEdit.Name;
                group.Description = groupEdit.Description;
                group.Weight = groupEdit.Weight;
                group.UpdatedDate = DateTime.Now;

                _context.Groups.Update(group);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPut("member/{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> UpdateMember(string id, [FromBody] GroupEditMemberViewModel groupEdit)
        {
            if (groupEdit == null) return BadRequest($"{nameof(groupEdit)} cannot be null");

            var group = await _context.Groups.FindAsync(id);
            if (group == null) return NotFound(id);

            switch (groupEdit.Type)
            {
                case "add":
                    group.Users.Add(_context.Users.Find(groupEdit.Member));
                    _context.Groups.Update(group);
                    await _context.SaveChangesAsync();
                    break;
                case "remove":
                    await _context.Database.ExecuteSqlRawAsync("DELETE FROM dbo.UserGroups WHERE UsersId = @id", new SqlParameter("@id", groupEdit.Member));
                    break;
            }

            return NoContent();
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> DeleteGroup(string id)
        {
            Group group = await _context.Groups.FindAsync(id);

            if (group == null) return NotFound(id);

            _context.Groups.Remove(group);

            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
