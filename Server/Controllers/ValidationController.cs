﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;

namespace SHAI.Controllers
{
    [Route("api/[controller]")]
    public class ValidationController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ApplicationDbContext _context;

        public ValidationController(ApplicationDbContext context, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
        }

        [HttpGet]
        public IActionResult Get(ValidationModel model)
        {
            int result;

            if (!string.IsNullOrEmpty(model.Id))
            {
                result = _context.ScalarIntValue.FromSqlRaw($"SELECT COUNT(*) AS Value FROM {model.Table} WHERE Id <> @id AND {model.Field} = @val", new[] { new SqlParameter("@id", model.Id), new SqlParameter("@val", model.Value) }).FirstOrDefaultAsync().Result.Value;
            }
            else
            {
                result = _context.ScalarIntValue.FromSqlRaw($"SELECT COUNT(*) AS Value FROM {model.Table} WHERE {model.Field} = @val", new SqlParameter("@val", model.Value)).FirstOrDefaultAsync().Result.Value;
            }
            return Ok(result <= 0);
        }
    }

    public class ValidationModel
    {
        public string Id { get; set; }
        public string Field { get; set; }
        public string Value { get; set; }
        public string Table { get; set; }
    }
}
