﻿using AutoMapper;

using SHAI.Core;
using SHAI.Core.Interfaces;
using SHAI.Models;

using IdentityServer4.AccessTokenValidation;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using SHAI.Authorization;
using SHAI.Helpers;
using SHAI.ViewModels;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DevExtreme.AspNet.Data;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Identity;
using System.Linq.Dynamic.Core;
using System.Data.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;

namespace SHAI.Controllers
{
    [Route("api/[controller]")]
    public class OrderController : ControllerBase
    {
        private IHubContext<ServerHub> _hub;
        private readonly IMapper _mapper;
        private readonly IAccountManager _accountManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IAuthorizationService _authorizationService;
        private readonly ILogger<AccountController> _logger;
        private readonly ApplicationDbContext _context;

        public OrderController(ApplicationDbContext context,
            IMapper mapper,
            IAccountManager accountManager,
            UserManager<ApplicationUser> userManager,
            IAuthorizationService authorizationService,
            ILogger<AccountController> logger,
            IHubContext<ServerHub> hub
            )
        {
            _hub = hub;
            _mapper = mapper;
            _accountManager = accountManager;
            _userManager = userManager;
            _authorizationService = authorizationService;
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        public IActionResult GetAll(DataSourceLoadOptions loadOptions)
        {
            return Ok(DataSourceLoader.Load(_context.Orders.Select(o => new OrderViewModel(o, _context)).ToList(), loadOptions));
        }

        [HttpPost]
        [ProducesResponseType(201, Type = typeof(OrderViewModel))]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        public async Task<IActionResult> Post([FromBody] OrderEditViewModel obj)
        {
            //if (!(await _authorizationService.AuthorizeAsync(this.User, (user.Roles, new string[] { }), Authorization.Policies.AssignAllowedRolesPolicy)).Succeeded)
            //    return new ChallengeResult();

            if (ModelState.IsValid)
            {
                if (obj == null) return BadRequest($"{nameof(Order)} cannot be null");

                var o = _context.Orders.FirstOrDefault(o => o.MemberId == obj.MemberId && o.ClassRoomId == obj.ClassRoomId);

                if(o != null) return BadRequest("Member has registered for this class !");

                var num = _context.ScalarIntValue.FromSqlRaw(@"SELECT ISNULL(CAST((SELECT TOP 1 * FROM STRING_SPLIT((SELECT TOP 1 Id FROM dbo.Orders c ORDER BY Id DESC) , '-')  ORDER BY value ASC) AS INTEGER), 0) AS Value").FirstOrDefault().Value;

                obj.Id = Utilities.PaddInt(num + 1, 4, "SHAI-OD-");

                _context.Orders.Add(new Order() { Id = obj.Id, ClassRoomId = obj.ClassRoomId, MemberId = obj.MemberId, Status = obj.Status });

                await _context.SaveChangesAsync();

                return NoContent();
            }
            else
            {
                return BadRequest(ModelState);
            }

        }

        [HttpPut("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Update(string id, [FromBody] OrderEditViewModel obj)
        {
            if (ModelState.IsValid)
            {
                if (obj == null) return BadRequest($"{nameof(Order)} cannot be null");

                var o = await _context.Orders.FindAsync(id);
                if (o == null) return NotFound(id);

                o.ClassRoomId = obj.ClassRoomId;
                o.MemberId = obj.MemberId;
                o.UpdatedDate = DateTime.Now;

                _context.Orders.Update(o);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> DeleteOrder(string id)
        {
            Order Order = await _context.Orders.FindAsync(id);

            if (Order == null) return NotFound(id);

            _context.Orders.Remove(Order);

            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
