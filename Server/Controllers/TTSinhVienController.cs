﻿using AutoMapper;

using SHAI.Core;
using SHAI.Core.Interfaces;
using SHAI.Models;

using IdentityServer4.AccessTokenValidation;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using SHAI.Authorization;
using SHAI.Helpers;
using SHAI.ViewModels;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DevExtreme.AspNet.Data;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Identity;
using System.Linq.Dynamic.Core;
using System.Data.Entity;
using Microsoft.EntityFrameworkCore;
using SHAI.DAL.Core.Interfaces;

namespace SHAI.Controllers
{
    //[Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    public class TTSinhVienController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        public TTSinhVienController(ApplicationDbContext context
          )
        {
            _context = context;
        }

        [HttpGet()]
        [ProducesResponseType(200, Type = typeof(List<SinhVienViewModel>))]
        public IActionResult Get(DataSourceLoadOptions loadOptions)
        {
            var sinhvien = _context.SinhViens.Select(o => new SinhVienViewModel(o)).ToArray();
            return Ok(sinhvien);
            //return Ok(DataSourceLoader.Load(_context.SinhViens.Select(o => new SinhVienViewModel(o)).ToArray(), loadOptions));
        }
        
        // GET api/<TTSinhVienController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        [HttpPost()]
        [ProducesResponseType(201, Type = typeof(SinhVienViewModel))]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        public async Task<IActionResult> Post([FromBody] SinhVienViewModel obj)
        {
            if (ModelState.IsValid)
            {
                if (obj == null) return BadRequest($"{nameof(SinhVien)} cannot be null");

                //var num = _context.ScalarIntValue.FromSqlRaw(@"SELECT ISNULL(CAST((SELECT TOP 1 * FROM STRING_SPLIT((SELECT TOP 1 Id FROM dbo.SinhViens c ORDER BY Id DESC) , '-')  ORDER BY value ASC) AS INTEGER), 0) AS Value").FirstOrDefault().Value;

                //obj.Id = Utilities.PaddInt(num + 1, 4, "SHAI-HY-");
                var sinhvien = _context.SinhViens.Select(o => new SinhVienViewModel(o)).ToArray();
                _context.SinhViens.Add(new SinhVien()
                {
                    Id = (sinhvien.Count() +1).ToString(),
                    HoTen = obj.HoTen,
                    Ngaysinh = obj.Ngaysinh,
                    SoThich = obj.SoThich,
                    GioiTinh = obj.GioiTinh,
                    HocLuc = obj.HocLuc,
                    SDT = obj.SDT,
                });

                await _context.SaveChangesAsync();
                return NoContent();
            }
            else
            {
                return BadRequest(ModelState);
            }
        }
        // PUT api/<TTSinhVienController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }
        [HttpDelete("{id}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Delete(string id)
        {
            Course Course = await _context.Courses.FindAsync(id);

            if (Course == null) return NotFound(id);

            _context.Courses.Remove(Course);

            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
