﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks; 

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using SHAI.Helpers;

namespace SHAI.Controllers
{
    [Route("api/[controller]")]
    public class UploadsController : ControllerBase
    {

        [HttpPost]
        [DisableRequestSizeLimit]
        [AllowAnonymous]
        public async Task<IActionResult> UploadFile()
        {
            var files = Request.Form.Files;
            var myUrl = $"{Request.Scheme}://{Request.Host}{Request.PathBase}";

            if (files == null || !files.Any()) return StatusCode(404, "Not have file");

            var result = new List<object>();
            var uploads = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "uploads", "temp");

            if (!Directory.Exists(uploads)) Directory.CreateDirectory(Path.GetDirectoryName(uploads));

            try
            {
                foreach (var file in files)
                {
                    var extension = Path.GetExtension(file.FileName).ToLower();
                    var fileName = $"{Guid.NewGuid()}{extension}";
                    var filePath = Path.Combine(uploads, fileName);
                    var mime = Mime.GetMimeType(fileName);

                    using (var stream = System.IO.File.Create(filePath))
                    {
                        await file.CopyToAsync(stream);

                        stream.Dispose();
                    }

                    result.Add(new
                    {
                        Name = file.FileName,
                        MimeType = mime,
                        Url = Path.Combine("/", "temp", fileName).Replace('\\', '/'),
                        TempUrl = filePath,
                        Size = new FileInfo(filePath).Length,
                    });
                }

                return Ok(result);
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }
    }

    public class UploadObject
    {
        public string FileName { get; set; }
        public string Folder { get; set; }
    }

    public class UploadResult
    {
        public string DisplayFileName { get; set; }
        public string OriginalFileName { get; set; }
        public string ServerPath { get; set; }
        public string TempPath { get; set; }
        public long FileSize { get; set; }
    }
}
