﻿using SHAI.Models;
using SHAI.ViewModels;

using IdentityServer4.Extensions;
using IdentityServer4.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using System.Linq.Dynamic.Core;

namespace SHAI.Helpers
{
    public static class StoreHelper
    {
        public static IEnumerable<StoreFilterViewModel> ParseFilters(object[] filterArray)
        {
            if (filterArray == null)
                return Enumerable.Empty<StoreFilterViewModel>();
            else
                return FlattenArray(filterArray).Select(f => new StoreFilterViewModel() { Field = f[0], Comparator = ParseComparator(f[1]), Value = f[2] }).ToList();
        }

        private static StoreFilterViewModel.FilterComparator ParseComparator(string pComparator)
        {
            if (pComparator == "=")
                return StoreFilterViewModel.FilterComparator.Equals;
            else if (pComparator == "contains")
                return StoreFilterViewModel.FilterComparator.Contains;
            else if (pComparator == ">")
                return StoreFilterViewModel.FilterComparator.GreaterThan;
            else if (pComparator == ">=")
                return StoreFilterViewModel.FilterComparator.GreaterThanOrEqual;
            else if (pComparator == "<")
                return StoreFilterViewModel.FilterComparator.LessThan;
            else if (pComparator == "<=")
                return StoreFilterViewModel.FilterComparator.LessThanOrEqual;
            else// if (pComparator == "!=")  
                return StoreFilterViewModel.FilterComparator.NotEquals;
        }

        private static IEnumerable<string[]> FlattenArray(object[] filters)
        {
            foreach (var filter in filters.Where(f => f.GetType() == typeof(object[])).Cast<object[]>()) //we drop all "and"s  
            {
                if (filter.All(f => f.GetType() != typeof(object[])))
                    yield return filter.Select(o => o.ToString()).ToArray();
                else
                    foreach (var subFilter in FlattenArray(filter))
                        yield return subFilter;
            }
        }

        public static bool PropertyExists<T>(string propertyName) => typeof(T).GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance) != null;
    }

    public static class LinqExtensions
    {
        public static IQueryable<TSource> ApplyFilters<TSource>(this IQueryable<TSource> query, IEnumerable<StoreFilterViewModel> Filters)
        {
            foreach (StoreFilterViewModel filterInfo in Filters)
            {
                if (PropertyExists<TSource>(filterInfo.Field))
                {
                    switch (filterInfo.Comparator)
                    {
                        case StoreFilterViewModel.FilterComparator.Contains:
                            query = query.Where($"{filterInfo.Field} LIKE %{filterInfo.Value}%");
                            break;
                        default:
                            query = query.Where($"{filterInfo.Field} {filterInfo.Comparator} {filterInfo.Value}");
                            break;
                    }
                    
                }
                //if (filterInfo.Field == "StringField")
                //    query = query.Where(d => d.StringField.ToLower() == filterInfo.Value.ToLower());
                //else if (filterInfo.Field == "Date")
                //{
                //    DateTime date;
                //    if (DateTime.TryParse(filterInfo.Value, out date))
                //    {
                //        if (filterInfo.Comparator == StoreFilterViewModel.FilterComparator.GreaterThanOrEqual)
                //            query = query.Where(d => d.Start >= date);
                //        else if (filterInfo.Comparator == StoreFilterViewModel.FilterComparator.LessThan)
                //            query = query.Where(d => d.Start < date);
                //    }
                //}
                //else if (filterInfo.Field == "Schedule")
                //    query = query.Where(d => d.Schedule.Name.ToLower() == filterInfo.Value.ToLower());
                //else if (filterInfo.Field == "Product")
                //    query = query.Where(d => d.Product.Name.ToLower() == filterInfo.Value.ToLower());
            }
            return query;
        }

        public static IQueryable<TSource> ApplySorting<TSource>(this IQueryable<TSource> query, IEnumerable<DataGridSortViewModel> Sortings)
        {
            foreach (var sortInfo in Sortings)
            { 
                if (!string.IsNullOrEmpty(sortInfo.Selector) && PropertyExists<TSource>(sortInfo.Selector))
                {
                    return sortInfo.Desc ? query.OrderByDescending(UpperFirst(sortInfo.Selector)) : query.OrderBy(UpperFirst(sortInfo.Selector));
                }
            }

            return (IQueryable<TSource>) query;
        }
         
        private static IQueryable<TSource> OrderBy<TSource>(this IQueryable<TSource> query, string propertyName)
        {
            var entityType = typeof(TSource);

            // Create x=>x.PropName
            var propertyInfo = entityType.GetProperty(propertyName);

            // If we try to order by a property that does not exist in the object return the list
            if (propertyInfo == null)
            {
                return (IQueryable<TSource>)query;
            }

            var arg = Expression.Parameter(entityType, "x");
            var property = Expression.Property(arg, propertyName);
            var selector = Expression.Lambda(property, new ParameterExpression[] { arg });

            // Get System.Linq.Queryable.OrderBy() method.
            var method = typeof(Queryable)
                .GetMethods()
                .Where(m => m.Name == "OrderBy" && m.IsGenericMethodDefinition) // ensure selecting the right overload  
                .Single(m => m.GetParameters().ToList().Count == 2);

            //The linq's OrderBy<TSource, TKey> has two generic types, which provided here
            MethodInfo genericMethod = method.MakeGenericMethod(entityType, propertyInfo.PropertyType);

            /* Call query.OrderBy(selector), with query and selector: x=> x.PropName
              Note that we pass the selector as Expression to the method and we don't compile it.
              By doing so EF can extract "order by" columns and generate SQL for it. */
            return (IQueryable<TSource>)genericMethod.Invoke(genericMethod, new object[] { query, selector });
        }

        private static IQueryable<TSource> OrderByDescending<TSource>(this IQueryable<TSource> query, string propertyName)
        {
            var entityType = typeof(TSource);

            // Create x=>x.PropName
            var propertyInfo = entityType.GetProperty(propertyName);

            // If we try to order by a property that does not exist in the object return the list
            if (propertyInfo == null)
            {
                return (IQueryable<TSource>)query;
            }

            var arg = Expression.Parameter(entityType, "x");
            var property = Expression.Property(arg, propertyName);
            var selector = Expression.Lambda(property, new ParameterExpression[] { arg });

            // Get System.Linq.Queryable.OrderBy() method.
            var method = typeof(Queryable)
                .GetMethods()
                .Where(m => m.Name == "OrderByDescending" && m.IsGenericMethodDefinition) // ensure selecting the right overload  
                .Single(m => m.GetParameters().ToList().Count == 2);

            //The linq's OrderBy<TSource, TKey> has two generic types, which provided here
            MethodInfo genericMethod = method.MakeGenericMethod(entityType, propertyInfo.PropertyType);

            /* Call query.OrderBy(selector), with query and selector: x=> x.PropName
              Note that we pass the selector as Expression to the method and we don't compile it.
              By doing so EF can extract "order by" columns and generate SQL for it. */
            return (IQueryable<TSource>)genericMethod.Invoke(genericMethod, new object[] { query, selector });
        }

        static string UpperFirst(string word)
        {
            //return System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(word.ToLower());
            return char.ToUpper(word[0]) + word[1..];
        } 

        public static bool PropertyExists<T>(string propertyName) => typeof(T).GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance) != null;
    }
}
