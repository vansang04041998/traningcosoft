﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.SignalR;

using SHAI.Helpers;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SHAI
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ServerHub : Hub
    {
        private static List<UserOnline> _userOnlines = new List<UserOnline>();

        public int ClientCount { get; set; }

        public int OnlineCount { get { return _userOnlines.Count; } } 

        public override async Task OnConnectedAsync()
        {
            if (Context.User.Identity.IsAuthenticated)
            {
                var userId = Utilities.GetUserId(Context.User);
                // Kiểm tra xem user mới hay đã đăng nhập ở trình duyệt hoặc tab khác
                // Chưa có thì mới thêm vào danh sách, có rồi thì chỉ thêm vào list để tránh trùng 
                var user = _userOnlines.FirstOrDefault(u => u.UserId == userId);

                if (user != null)
                {
                    user.Add(Context.ConnectionId, Context.Features.Get<IHttpConnectionFeature>()?.RemoteIpAddress.ToString());
                    await Groups.AddToGroupAsync(Context.ConnectionId, $"user-{user.UserId}");
                }
                else
                {
                    user = new UserOnline { UserId = Utilities.GetUserId(Context.User), UserName = Context.User.Identity.Name };

                    user.Add(Context.ConnectionId, Context.Features.Get<IHttpConnectionFeature>()?.RemoteIpAddress.ToString());

                    _userOnlines.Add(user);
                    await Groups.AddToGroupAsync(Context.ConnectionId, $"user-{user.UserId}");
                    await Clients.All.SendAsync("SystemNotification", new { type = "UserUpdate", message = $"{user.UserName} is online !", users = _userOnlines.Select(o => o.UserName) });
                }
                //TODO : lưu log đăng nhập ở đây
            }
            else
            {
                await Groups.AddToGroupAsync(Context.ConnectionId, "Anonymous Users");
            }

            // Tăng số lượng user online
            ClientCount++;

            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            var user = GetUserById(Context.ConnectionId);

            if (user != null)
            {
                user.Remove(Context.ConnectionId);
                await Groups.RemoveFromGroupAsync(Context.ConnectionId, $"user-{user.UserId}");
                // Nếu đã đóng hết các connection thì xóa user khỏi danh sách online
                if (user.Connections.Count == 0)
                {
                    _userOnlines.Remove(user); 

                    await Clients.All.SendAsync("SystemNotification", new { message = $"{user.UserName} is offline !", users = _userOnlines.Select(o => o.UserName) });
                    //TODO: ghi Log Logout
                }
            }
            else
            {
                await Groups.RemoveFromGroupAsync(Context.ConnectionId, "Anonymous Users");
            }

            // Giảm số lượng user online
            if (ClientCount > 0) ClientCount--;

            await base.OnDisconnectedAsync(exception);
        }

        //public void SendChatMessage(string who, string message)
        //{
        //    string name = Context.User.Identity.Name;

        //    foreach (var connectionId in _connections.GetConnections(who))
        //    {
        //        Clients.Client(connectionId).addChatMessage(name + ": " + message);
        //    }
        //}

        //public void Login(string id)
        //{
        //    //string name = Context.User.Identity.Name;

        //    //foreach (var connectionId in _connections.GetConnections(who))
        //    //{
        //    //    Clients.Client(connectionId).addChatMessage(name + ": " + message);
        //    //}
        //    _userManager.
        //}

        private UserOnline GetUserById(string id) => _userOnlines.FirstOrDefault(u => u.Connections.Any(c => c.Id == id));
    }

    public partial class UserOnline
    {
        public string UserId { get; set; }
        public string UserName { get; set; }

        public int Count
        {
            get
            {
                lock (_connections)
                {
                    return _connections.Count;
                }
            }
        }
        private readonly List<UserConnection> _connections = new List<UserConnection>();

        public List<UserConnection> Connections
        {
            get
            {
                lock (_connections)
                {
                    return _connections;
                }
            }
        }

        public void Add(string id, string ip = "")
        {
            lock (_connections)
            {
                if (_connections.Count == 0)
                {
                    _connections.Add(new UserConnection { Id = id, Ip = ip, ConnectedTime = DateTime.Now });
                }
                else
                {
                    lock (_connections)
                    {
                        if (_connections.All(c => c.Id != id))
                        {
                            _connections.Add(new UserConnection { Id = id, Ip = ip, ConnectedTime = DateTime.Now });
                        }
                    }
                }
            }
        }
        public void Remove(string id)
        {
            lock (_connections)
            {
                if (_connections.Count > 0)
                {
                    _connections.RemoveAll(c => c.Id == id);
                }
            }
        }
    }

    public class UserConnection
    {
        public string Id { get; set; }
        public string Ip { get; set; } 
        public DateTime ConnectedTime{ get; set; }
    }

}