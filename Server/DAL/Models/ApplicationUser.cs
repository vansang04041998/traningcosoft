﻿using Microsoft.AspNetCore.Identity;

using SHAI.Models.Interfaces;

using System;
using System.Collections.Generic;

namespace SHAI.Models
{
    public class ApplicationUser : IdentityUser, IAuditableEntity
    {
        //public virtual string FriendlyName
        //{
        //    get
        //    {
        //        string friendlyName = string.IsNullOrWhiteSpace(FullName) ? UserName : FullName;

        //        if (!string.IsNullOrWhiteSpace(JobTitle)) friendlyName = $"{JobTitle} {friendlyName}";

        //        return friendlyName;
        //    }
        //}  
        public ApplicationUser()
        {
            this.ClassRooms = new HashSet<ClassRoom>();
            this.Groups = new HashSet<Group>();
        }

        public string JobTitle { get; set; }
        public string FullName { get; set; }
        public string ChineseName { get; set; }
        public DateTime BirthDate { get; set; }
        public string Address { get; set; }
        public string Configuration { get; set; }
        public string Gender { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsStaff { get; set; }
        public bool IsLockedOut => this.LockoutEnabled && this.LockoutEnd >= DateTimeOffset.UtcNow;

        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime JoinDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

        public int? MemberShip { set; get; }
        public DateTime? MemberShipStartDate { get; set; }
        public DateTime? ExpirationDate { get; set; }

        public virtual ICollection<IdentityUserRole<string>> Roles { get; set; }
        public virtual ICollection<IdentityUserClaim<string>> Claims { get; set; }
        public virtual ICollection<ClassRoom> ClassRooms { get; set; }
        public virtual ICollection<Group> Groups { get; set; }
        //public virtual MembershipFee MembershipFee { get; internal set; }
    }

}
