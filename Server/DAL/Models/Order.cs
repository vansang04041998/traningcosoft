﻿using Microsoft.AspNetCore.Identity;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace SHAI.Models
{
    public class Order : AuditableEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; } 
        public string ClassRoomId { get; internal set; }
        public string MemberId { get; internal set; }

        public int Status { get; internal set; }

        [JsonIgnore]
        public virtual ClassRoom ClassRoom { get; set; }

        [JsonIgnore]
        public virtual ApplicationUser Member { get; set; }
    }
}
