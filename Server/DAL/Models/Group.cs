﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SHAI.Models
{
    public class Group : AuditableEntity
    {
        public Group()
        {
            Id = Guid.NewGuid().ToString();
            Users = new List<ApplicationUser>();
        } 

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Weight { get; set; } 
        public virtual ICollection<ApplicationUser> Users { get; set; }
    }

    public partial class UserGroup
    {  
        public string GroupsId { get; set; }
        public string UsersId { get; set; }
    }
}
