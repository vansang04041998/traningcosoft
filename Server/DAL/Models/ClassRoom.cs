﻿using Microsoft.AspNetCore.Identity;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace SHAI.Models
{
    public class ClassRoom : AuditableEntity
    {
        public ClassRoom()
        {
            this.Users = new HashSet<ApplicationUser>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }

        public string CourseId { get; set; }

        public string Name { get; set; }
        public string Lecturers { get; set; }
        public string Code { get; set; }
        public string Venue { get; set; }
        public string Description { get; set; }

        public int Fee { get; set; }

        public DateTime ScheduleDate { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }

        public int Status {  get; set; }

        [JsonIgnore]
        public virtual Course Course { get; set; }
        public virtual ICollection<ApplicationUser> Users { get; set; }
    }
}
