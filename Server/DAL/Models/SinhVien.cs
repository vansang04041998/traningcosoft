﻿using Microsoft.AspNetCore.Identity;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SHAI.Models
{
    public class SinhVien
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }
        public string HoTen { get; set; }
        public DateTime Ngaysinh { get; set; }
        public int? GioiTinh { get; set; }
        public string SoThich { get; set; }
        public string HocLuc { get; set; }
        public int? SDT { get; set; }
    }
}
