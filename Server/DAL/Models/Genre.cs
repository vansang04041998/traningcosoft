﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SHAI.Models
{
    public class Genre : AuditableEntity
    {
        [Key]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Weight { get; set; } 

        [NotMapped]
        public virtual ICollection<Course> Courses { get; set; }
    }
}
