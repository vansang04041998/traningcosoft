﻿using Microsoft.AspNetCore.Identity;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace SHAI.Models
{
    public class Course : AuditableEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; } 
        public string GenreId { get; internal set; } 
        public string Name { get; set; }
        public string ChineseName { get; set; }
        public string Image { get; set; }
        public string Code { get; set; }
        public string Requirements {  get; set; }
        public string Language { get; set; }
        public int QuotaPerLesson { get; set; }
        public string Description { get; set; } 

        public int NumberOfLesson { get; set; }

        public virtual ICollection<ClassRoom> ClassRooms { get; set; }

        [JsonIgnore]
        public virtual Genre Genre { get; set; }
    }
}
