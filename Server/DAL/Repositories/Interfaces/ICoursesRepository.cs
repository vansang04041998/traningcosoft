﻿using SHAI.Models;

namespace SHAI.Repositories.Interfaces
{
    public interface ICoursesRepository : IRepository<Course>
    {

    }
}
