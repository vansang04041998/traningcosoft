﻿using SHAI.Models;
using SHAI.Repositories.Interfaces;

using Microsoft.EntityFrameworkCore;

namespace SHAI.Repositories
{
    public class CourseRepository : Repository<Course>, ICoursesRepository
    {
        public CourseRepository(DbContext context) : base(context)
        { }

        private ApplicationDbContext _appContext => (ApplicationDbContext)_context;
    }
}
