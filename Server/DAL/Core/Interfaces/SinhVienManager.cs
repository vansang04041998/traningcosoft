﻿using SHAI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SHAI.DAL.Core.Interfaces
{
    public interface SinhVienManager
    {
        Task<List<(SinhVien SinhVien, string[] Roles)>> GetSinhVien(int page, int pageSize);
        Task<List<(ApplicationUser User, string[] Roles)>> GetUsersAndRolesAsync(int page, int pageSize);

    }
}
