﻿using SHAI.Repositories;
using SHAI.Repositories.Interfaces;

namespace SHAI
{
    public class UnitOfWork : IUnitOfWork
    {
        readonly ApplicationDbContext _context;

        //ICustomerRepository _customers;
        //IProductRepository _products;
        //IOrdersRepository _orders;
        ICoursesRepository _courses;
         
        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
        }

        public ICoursesRepository Courses
        {
            get
            {
                if (_courses == null)
                    _courses = new CourseRepository(_context);

                return _courses;
            }
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }
    }
}
