﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using SHAI.Core;
using SHAI.Core.Interfaces;
using SHAI.DAL.Models;
using SHAI.Models;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SHAI
{
    public interface IDatabaseInitializer
    {
        Task SeedAsync();
    }

    public class DatabaseInitializer : IDatabaseInitializer
    {
        private readonly ApplicationDbContext _context;
        private readonly IAccountManager _accountManager;
        private readonly ILogger _logger;

        public DatabaseInitializer(ApplicationDbContext context, IAccountManager accountManager, ILogger<DatabaseInitializer> logger)
        {
            _accountManager = accountManager;
            _context = context;
            _logger = logger;
        }

        public async Task SeedAsync()
        {
            await _context.Database.MigrateAsync().ConfigureAwait(false);

            if (!await _context.Users.AnyAsync())
            {
                _logger.LogInformation("Generating inbuilt accounts");

                //var g = new Genre()
                //{
                //    Name = "Default",
                //    Weight = 1,
                //    Description = "Static Genre",
                //    Courses = new List<Course>()
                //};

                //_context.Genres.Add(g);

                //g.Courses.Add(new Course()
                //{
                //    Name = "Default",
                //    Code = "ABC",
                //    NumberOfLesson = 10,
                //});

                _context.Groups.Add(new Group() { Id = "SHAI-GROUP-0001", Name = "Default Group", Weight = 0 });

                _context.Genres.Add(new Genre() { Id = "SHAI-GENRE-0001", Name = "Children", Weight = 0 });
                _context.Genres.Add(new Genre() { Id = "SHAI-GENRE-0002", Name = "Adult", Weight = 1 });

                _context.Courses.Add(new Course() { Id = "SHAI-HY-0001", ChineseName = "儿童基础", Name = "Children Foundation", Requirements = "NIL", Language = "Chinese", NumberOfLesson = 1, GenreId = "SHAI-GENRE-0002", QuotaPerLesson = 5 });
                _context.Courses.Add(new Course() { Id = "SHAI-HY-0002", ChineseName = "成人基础 1&2", Name = "Adult Foundation 1 & 2 ", Requirements = "AUDITION REQUIRED", NumberOfLesson = 2, Language = "Chinese", QuotaPerLesson = 5 });

                await _context.SaveChangesAsync();

                const string adminRoleName = "administrator";
                const string userRoleName = "user";

                await EnsureRoleAsync(adminRoleName, "Default administrator", ApplicationPermissions.GetAllPermissionValues());
                await EnsureRoleAsync(userRoleName, "Default user", new string[] { });

                await CreateUserAsync("admin", "123456aA@", "Administrator", "cuongnm@cosoft.vn", "0916110785", "Male", new string[] { adminRoleName });
                await CreateUserAsync("user", "123456aA@", "Standard User", "user@cosoft.vn", "", "Female", new string[] { userRoleName });

                await CreateUserAsync("SHA-USER-0001", "123456aA@", "Dancer 1", "dancer1@testemail.com", "123456789", "Female", new string[] { }, false);

                _logger.LogInformation("Inbuilt account generation completed");
            }
        }

        private async Task EnsureRoleAsync(string roleName, string description, string[] claims)
        {
            if ((await _accountManager.GetRoleByNameAsync(roleName)) == null)
            {
                ApplicationRole applicationRole = new ApplicationRole(roleName, description);

                var result = await this._accountManager.CreateRoleAsync(applicationRole, claims);

                if (!result.Succeeded)
                    throw new Exception($"Seeding \"{description}\" role failed. Errors: {string.Join(Environment.NewLine, result.Errors)}");
            }
        }

        private async Task<ApplicationUser> CreateUserAsync(string userName, string password, string fullName, string email, string phoneNumber, string gender, string[] roles, bool isStaff = true)
        {
            ApplicationUser applicationUser = new ApplicationUser
            {
                UserName = userName,
                FullName = fullName,
                Email = email,
                PhoneNumber = phoneNumber,
                EmailConfirmed = true,
                Gender = gender,
                IsEnabled = true,
                IsStaff = isStaff,
            };

            var result = await _accountManager.CreateUserAsync(applicationUser, roles, password);

            if (!result.Succeeded)
                throw new Exception($"Seeding \"{userName}\" user failed. Errors: {string.Join(Environment.NewLine, result.Errors)}");


            return applicationUser;
        }
    }
}
